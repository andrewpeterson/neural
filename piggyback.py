#!/usr/bin/env python
"""This module contains a 'piggyback' machine-learning calculator, that
sits on top of a full accuracy calculator. The user attaches a specified
number (e.g., 5) of piggybacks. When a call is made to
'get_potential_energy' or 'get_forces', the calculator first queries the
piggybacks. If they are in agreement, we can be reasonably confident that
their answer will match the full accuracy calculator. If they are not in
agreement, the full accuracy calculator is called instead. The new data
point is used to improve the machine learning calculators."""

import os
import numpy as np
from datetime import datetime

from ase import io
from ase.calculators.calculator import Calculator, all_changes
from ase.parallel import paropen

from neural.utilities import UntrainedError, ExtrapolateError


class TestCalc(Calculator):

    implemented_properties = ['energy', 'forces']

    def __init__(self, atoms=None):
        Calculator.__init__(self, atoms=atoms)

    def calculate(self, atoms=None, properties=['energy'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        for item in properties:
            print('%s requested.' % item)
        print(system_changes)

        for item in properties:
            self.results[item] = 0.


class Piggyback(Calculator):
    """
    A machine-learning calculator that sits on top of another (typically
    full-accuracy) calculator. When the piggybacks are in agreement with
    one another, they are assumed to be giving the correct answer and their
    answer is taken. When they disagree, the full-accuracy calculator is
    employed instead and the result of the full-accuracy calculation is
    added to the training set for the piggyback calculators, thus improving
    the ability to predict for future cases.

    One can determine if the last energy was predicted or actual based on
    checking the value (True or False) of self.predicted.

    Parameters:

        real_calculator : ASE calculator object
            An initialized real calculator, such as GPAW(...).
        piggybacks : list
            A list of initialized  machine-learning calculator objects,
            such as [Neural(...), Neural(...), ...].
        atoms : ASE atoms object
        trainingdata : filename
            path to trajectory file at which to store the data. If it does
            not exist, will create.
        label : string
            Calculator name for saving files.
        check : object
            Optional customized instance to check if machine-learning
            calculators are consistent with one another. Customize by
            importing and instantiating
            neural.piggyback.ConsistencyCheck (or writing your own).
        retrain_interval : integer
           The piggybacks will not re-train every time a value is added,
           but after this many values are added.
    """

    implemented_properties = ['energy', 'forces']

    def __init__(self, real_calculator, piggybacks, atoms=None,
                 trainingdata='training.traj', label=None,
                 check=None, retrain_interval=1):
        Calculator.__init__(self, atoms=atoms, label=label)
        self._min_images = 5
        #FIXME May actually want the tolerance to be a callable function
        # such that it can be force-adaptive. E.g., if fmax is large it can
        # be a larger tolerance.
        #FIXME: May want to idiotproof/check to make sure that the user
        # doesn't feed in the list of pointers to the same Neural object,
        # as would happen with [Neural(...)]*5.
        self._ftol = 0.01  # eV / AA

        self._trained = False
        self._real_calculator = real_calculator
        self._piggybacks = piggybacks
        self._trajfile = trainingdata
        if not check:
            check = ConsistencyCheck()
        self._checkconsistency = check
        self.predicted = None
        self._retrain_interval = retrain_interval

        self._retrain()

    def calculate(self, atoms=None, properties=['energy'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)
        atoms = self.atoms

        for item in properties:
            self.log('%s requested.' % item)
        for item in system_changes:
            self.log(' %s changed.' % item)

        if not self._trained:
            self.log(' Piggybacks not trained.')
            self._calculate_untrained(atoms)
            return

        self.log(' Piggyback predictions:')
        try:
            energies = [calc.get_potential_energy(atoms) for calc in
                        self._piggybacks]
        except (UntrainedError, ExtrapolateError):
            self.log(' Outside previous range. Retraining.')
            self._calculate_untrained(atoms)
            return

        passed = self._checkconsistency(energies, self.log)
        if not passed:
            self.log(' Trained values inconsistent.')
            self._calculate_untrained(atoms)
            return

        energy = np.average(energies)
        self.log(' Using trained energy %.4f.' % energy)
        self.results['energy'] = energy
        self.predicted = True
        self.log(' DEBUG: real energy is %.4f' %
                 self._real_calculator.get_potential_energy(atoms))

        if False:
            if 'forces' in properties:
                self.log(' Getting forces.')
                print(atoms)
                allforces = [calc.get_forces(atoms) for calc in
                             self._piggybacks]
                # Note can check largest span with:
                #  (np.max(allforces, axis=0) - np.min(allforces,
                #   axis=0)).max()
                forces = np.average(allforces, axis=0)
                self.log(' Using trained forces.')
                self.results['forces'] = forces

    def _calculate_untrained(self, atoms):
        """Use the real (e.g., DFT) calculate to get energy and forces.
        Adds the atoms object and its energy to the training file and
        retrains the piggybacks. Returns the real energy and forces."""
        energy = self._real_calculator.get_potential_energy(atoms)
        forces = self._real_calculator.get_forces(atoms)
        self.log(' Using real energy: %.4f' % energy)
        self.log(' Using real forces.')
        self.results['energy'] = energy
        self.results['forces'] = forces
        self.predicted = False
        # Retrain.
        traj = io.Trajectory(self._trajfile, 'a')
        traj.write(self._real_calculator.get_atoms())
        traj.close()
        self._retrain()

    def _retrain(self):
        """Retrains the machine learning piggybacks."""
        if not os.path.exists(self._trajfile):
            return
        traj = io.Trajectory(self._trajfile, 'r')
        if len(traj) < self._min_images:
            self.log(' Not training, only %i images in training file'
                     ' (%i needed).' % (len(traj), self._min_images))
            traj.close()
            self._trained = False
            return
        if len(traj) % self._retrain_interval != 0:
            self.log(' Only retraining on multiples of %i. (Current: %i)'
                     % (self._retrain_interval, len(traj)))
            traj.close()
            return
        tic = datetime.now()
        for index, calc in enumerate(self._piggybacks):
            self.log('  Training calculator #%i.' % index)
            # FIXME. This is a kludge -- should fix the train methods to be
            # consistent between the two calculators.
            try:
                calc.train(images=traj)
            except TypeError:
                calc.train(images=traj, errorlog='trainlog.txt')
        toc = datetime.now()
        traj.close()
        self.log(' (Re)trained. Training time %.2f min.' %
                 ((toc - tic).total_seconds() / 60.))
        self._trained = True

    def log(self, message):
        """Writes message to file."""
        if not self.label:
            return
        f = paropen(os.path.join(self.directory, '%s.log' % self.prefix),
                    'a')
        f.write(message + '\n')
        f.close()


class ConsistencyCheck:
    """Class to check if trained values are consistent enough with one
    another to use the piggyback prediction instead of recalculating the
    real value.

    A user can create a custom version of this -- it just needs to take the
    energies (and ultimately forces) and return True to use the piggyback
    answer or False to calculate a real version.

    energytol : float
        maximum span (eV) that the energies predicted by the piggyback
        calculators can cover. If exceeded, False is returned.
    log : function
        A function that when called, writes the message to the desired
        location. Default is not to log.
    """

    def __init__(self, energytol=0.01):
        self._energytol = energytol

    def __call__(self, energies, log=None):
        if not log:
            log = self.null

        span = max(energies) - min(energies)
        log((' [%.4f] ' % np.average(energies)) +
            ' '.join([('%.4f' % energy) for energy in energies]))
        if span > self._energytol:
            log(' Span %.4f > tol %.4f' % (span, self._energytol))
            return False
        else:
            log(' Span %.4f < tol %.4f' % (span, self._energytol))

        return True

    def null(self, _):
        pass
