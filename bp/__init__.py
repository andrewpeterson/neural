"""
Python code for neural network potentials
using the symmetry functions developed by
Behler and Parrinello (Phys. Rev. Lett., 2007, 98, 146401;
Phys. Chem. Chem. Phys., 2011, 13, 17930-17955;
Phys. Rev. B, 2012, 86, 079914) as inputs to the neural network.

Written by Andrew A. Peterson and Alireza Khorshidi (2015)
School of Engineering, Brown University, Providence, RI, 02912

"""

######################################################################

import numpy as np
from scipy.optimize import fmin_bfgs as optimizer
from datetime import datetime
import tempfile
from ase.calculators.neighborlist import NeighborList
from ase.calculators.calculator import Calculator
from ase.data import atomic_numbers
from ase.parallel import paropen
from collections import OrderedDict
from ase import io
import os
import json
import warnings
import gc
import multiprocessing as mp
from ..utilities import hash_image, Logger
try:
    from .. import fmodules  # version 7 of fmodules
    fmodules_version = 7
except ImportError:
    fmodules = None
from multiprocessing.managers import BaseManager

##############################################################################


class FmodulesManager(BaseManager):
    pass

# Class which wraps fmodules to create a shared object


class FmodulesWrapper(object):

    def run(self,
            proc,
            variables,
            len_of_variables,
            activation,):

        result = \
            fmodules.share_cost_function_task_between_cores(
                proc=proc,
                variables=variables,
                len_of_variables=len_of_variables,
                activation=activation,)
        return result

FmodulesManager.register('fmodules_', FmodulesWrapper)


##############################################################################


class BPNeural(Calculator):

    """Behler-Parrinello Neural Network Potential Calculator.
    Inputs:
        load: string
            load an existing (trained) BPNeural calculator from this
            path
        cutoff: float (default 6.5 Angstroms)
                  Radius above which neighbor interactions are ignored.
        Gs: dictionary of symbols and lists of dictionaries for making
        symmetry functions. Either auto-genetrated, or given in the following
        form: Gs = {"O": [..., ...], "Au": [..., ...]} where
        ... = {"type":"G2", "element":"O", "eta":0.0009} or
        ... = {"type":"G4", "elements":["O", "Au"], "eta":0.0001,
        "gamma":0.1, "zeta":1.0}
        hiddenlayers: architecture of hidden layers of the conventional
                    neural network (NN). Feed as either a tuple (e.g,
                    (5,5)), or a dictionary with one tuple per element key.
                    Note that for each atom, number of nodes in the input
                    layer is always equal to the number of symmetry
                    functions (Gs), and the number of nodes in the output
                    layer is always one. For example,
                    hiddenlayers = {"O":(3,5), "Au":(5,6)} means that for "O"
                    we have two hidden layers, the first one with three nodes
                    and the second one having five nodes.
        activation: string to assign the type of activation funtion. "linear"
                    refers to linear function, "tanh" refers to tanh
                    function, and "sigmoid" refers to sigmoid function.
        label: string
            default prefix / location used for all files.
        dblabel: string
            optional separate prefix (location) for database files,
            including fingerprints, fingerprint derivatives, and
            neighborlists. This file location can be shared between
            calculator instances to avoid re-calculating redundant
            information. If not supplied, just uses the value from
            label.
        weights: dictionary of symbols and dictionaries of arrays of weights;
                 each symbol and related dictionary corresponds to one chemical
                   element and one conventional neural network.  The weights
                   dictionary for the above example has the dimensions,
                   weights = {"O": {1: np.array(149,3), 2: np.array(4,5),
                   3: np.array(6,1)}, "Au": {1: np.array(148,5),
                   2: np.array(6,6), 3:np.array(7,1)}. The arrays are set up to
                   connect node i in the previous layer with node j in the
                   current layer with indices w[i,j]. There are n+1 rows
                   (i values), with the last row being the bias, and m columns
                   (j values). If weights is not given, the arrays will be
                   randomly generated from values between -0.5 and 0.5.
        scalings: dictionary of variables for slope and intercept for each
                    element. They are used in order to remove the final value
                    from the range that the activation function would
                    otherwise be locked in. For example,
                      scalings={"Pd":{"slope":2., "intercept":1.},
                                "O":{"slope":3., "intercept":4.}}
        fingerprints_range: range of fingerprints of each chemical species.
                            Should be fed as a dictionary of chemical species
                            and a list of minimum and maximun, e.g.
                            fingerprints_range={"Pd": [0.31, 0.59],
                                "O":[0.56, 0.72]}
        fingerprints_tag: a tag for identifying the functional form of
                            fingerprints used in the code
        extrapolate: boolean
            If True, allows for extrapolation, if False, does not allow.
        fortran: boolean
            If True, will use the fortran subroutines, else won't.
    Output:
        energy: float
        forces: matrix of floats

    **NOTE: The dimensions of the weight matrix should be consistent with the
            hiddenlayers.**
    """
    implemented_properties = ['energy', 'forces']

    # default_parameters should only include things necessary to reproduce
    # the calculation, such as the weights, cutoff, etc. Things like
    # whether to user fortran, verbosity of output, or how many cores to
    # train on should not be in this dictionary. Also training parameters
    # such as the force_coefficient should not be included.
    default_parameters = {'cutoff': 6.5,
                          'Gs': None,
                          'hiddenlayers': (5, 5),
                          'activation': 'tanh',
                          'weights': None,
                          'scalings': None,
                          'fingerprints_range': None,
                          'fingerprints_tag': 1,
                          }

    ##########################################################################

    def __init__(self, load=None, label=None, dblabel=None,
                 extrapolate=True, fortran=True, **kwargs):

        self.extrapolate = extrapolate
        self.fortran = fortran
        self.dblabel = dblabel
        if not dblabel:
            self.dblabel = label

        if self.fortran and not fmodules:
            raise RuntimeError('Not using the Fortran 90 part of the code. '
                               'Either compile fmodules as described in the '
                               'README for improved performance, or '
                               'initialize calculator with fortran=False.')

        if self.fortran and fmodules:
            wrong_version = fmodules.check_version(version=fmodules_version)
            if wrong_version:
                raise RuntimeError('Fortran part is not updated. Recompile'
                                   'with f2py as described in the README. '
                                   'Correct version is %i.'
                                   % fmodules_version)

        # Reading parameters from existing file if any:
        if load:
            try:
                json_file = paropen(load, 'rb')
            except IOError:
                json_file = paropen(make_filename(load,
                                                  'trained-parameters.json'),
                                    'rb')

            parameters = load_parameters(json_file)

            kwargs['cutoff'] = parameters['cutoff']
            kwargs['Gs'] = parameters['Gs']
            kwargs['hiddenlayers'] = parameters['hiddenlayers']
            kwargs['activation'] = parameters['activation']
            kwargs['weights'] = parameters['weights']
            kwargs['scalings'] = parameters['scalings']
            kwargs['fingerprints_range'] = parameters['fingerprints_range']
            kwargs['fingerprints_tag'] = parameters['fingerprints_tag']

        # Checking the compatibility of the forms of Gs, hiddenlayers and
        # weights.
        string1 = 'Gs and weights are not compatible.'
        string2 = 'hiddenlayers and weights are not compatible.'
        if 'hiddenlayers' in kwargs:
            if isinstance(kwargs['hiddenlayers'], dict):
                self.elements = sorted(kwargs['hiddenlayers'].keys())
                for element in self.elements:
                    if kwargs['weights'] is not None:
                        if 'Gs' in kwargs:
                            if np.shape(kwargs['weights'][element][1])[0] != \
                                    len(kwargs['Gs'][element]) + 1:
                                raise RuntimeError(string1)
                        if isinstance(kwargs['hiddenlayers'][element], int):
                            if np.shape(kwargs['weights'][element][1])[1] != \
                                    kwargs['hiddenlayers'][element]:
                                raise RuntimeError(string2)
                        else:
                            if np.shape(kwargs['weights'][element][1])[1] != \
                                    kwargs['hiddenlayers'][element][0]:
                                raise RuntimeError(string2)
                            for i in range(2,
                                           len(kwargs['hiddenlayers']
                                               [element]) + 1):
                                if (np.shape(kwargs['weights']
                                             [element][i])[0] !=
                                        kwargs['hiddenlayers'][
                                            element][i - 2] + 1 or
                                        np.shape(kwargs['weights']
                                                 [element][i])[1] !=
                                        kwargs['hiddenlayers']
                                        [element][i - 1]):
                                    raise RuntimeError(string2)
        del string1
        del string2

        Calculator.__init__(self, label=label, **kwargs)

        p = self.parameters
        # Checking that the activation function is given correctly:
        string = ('Unknown activation function; activation must be one of '
                  '"linear", "tanh", or "sigmoid".')
        if p.activation not in ['linear', 'tanh', 'sigmoid']:
            raise NotImplementedError(string)
        del string

        # Checking if the functional forms of fingerprints in the train set
        # is the same as those of the current version of the code:
        if p.fingerprints_tag != 1:
            raise FingerprintsError('Functional form of fingerprints has been '
                                    'changed. Re-train you train images set, '
                                    'and use the new weights and scalings.')

    #########################################################################

    def set(self, **kwargs):
        """Function to set parameters."""
        changed_parameters = Calculator.set(self, **kwargs)
        # FIXME. Decide whether to call reset. Decide if this is
        # meaningful in our implementation!
        if len(changed_parameters) > 0:
            self.reset()

    #########################################################################

    def set_label(self, label):
        """Sets label, ensuring that any needed directories are made."""

        Calculator.set_label(self, label)

        # Create directories for output structure if needed.
        if self.label:
            if (self.directory != os.curdir and
                    not os.path.isdir(self.directory)):
                os.makedirs(self.directory)

    #########################################################################

    def initialize(self, atoms):
        self.par = {}
        self.rc = 0.0
        self.numbers = atoms.get_atomic_numbers()
        self.forces = np.empty((len(atoms), 3))
        self.nl = NeighborList([0.5 * self.rc + 0.25] * len(atoms),
                               self_interaction=False)

    #########################################################################

    def calculate(self, atoms, properties, system_changes):
        """ Calculation of the energy of the system and forces of all atoms."""
        Calculator.calculate(self, atoms, properties, system_changes)

        p = self.parameters
        self.cutoff = p.cutoff

        if p.weights is None:
            raise RuntimeError("Calculator not trained; can't return "
                               'properties.')
        elif not self.elements:
            self.elements = sorted(p.weights.keys())

        if 'numbers' in system_changes:
            self.initialize(atoms)

        self.nl.update(atoms)
        # FIXME: What is the difference between the two updates on the top
        # and bottom? Is the one on the top necessary? Where is self.nl coming
        # from?

        # Update the neighborlist for making fingerprint. Used if atoms
        # position has changed.
        _nl = NeighborList(cutoffs=([self.cutoff / 2.] *
                                    len(atoms)),
                           self_interaction=False,
                           bothways=True,
                           skin=0.)
        _nl.update(atoms)

        cf = CalculateFingerprints(p.cutoff, p.Gs, atoms, _nl, self.fortran)
        ff = FeedForward(p.hiddenlayers, p.weights,
                         p.scalings, p.activation, self.fortran)

        # If fingerprints_range is not available, it will raise an error.
        if p.fingerprints_range is None:
            raise RuntimeError('The keyword "fingerprints_range" is not '
                               'available. It can be provided to the '
                               'calculator either by introducing a JSON file, '
                               'or by directly feeding the keyword to the '
                               'calculator. If you do not know the values but '
                               'still want to run the calculator, initialize '
                               'it with fingerprints_range="auto".')

        # If fingerprints_range is not given either as a direct keyword or as
        # the josn file, but instead is given as 'auto', it will be calculated
        # here.
        if p.fingerprints_range == 'auto':
            warnings.warn('The values of "fingerprints_range" are not given. '
                          'The user is expected to understand what is being '
                          'done!')
            p.fingerprints_range = \
                calculate_fingerprints_range(cf, self.elements, atoms)
        # Deciding on whether it is exptrapoling or interpolating is possible
        # only when fingerprints_range is provided by the user.
        elif self.extrapolate is False:
            if compare_train_test_fingerprints(
                    cf,
                    atoms,
                    p.fingerprints_range) == 1:
                raise ExtrapolateError('Trying to extrapolate, which'
                                       ' is not allowed. Change to '
                                       'extrapolate=True if this is'
                                       ' desired.')

        if properties == ['energy']:

            if self.fortran and fmodules:
                send_image_energy_data_to_f90_code(atoms, cf, self.elements,
                                                   p.hiddenlayers,
                                                   p.fingerprints_range)

                ravel = RavelVariables(p.weights, p.scalings)
                variables = ravel.to_vector(p.weights, p.scalings)
                self.energy = fmodules.get_potential_energy(
                    variables=variables,
                    len_of_variables=len(variables),
                    activation=p.activation)
            else:

                self.energy = 0.0
                for atom in atoms:
                    index = atom.index
                    symbol = atom.symbol
                    indexfp = cf.index_fingerprint(symbol, index)
                    # fingerprints are scaled to [-1, 1] range
                    scaled_indexfp = [None] * len(indexfp)
                    count = 0
                    for _ in range(len(indexfp)):
                        if (p.fingerprints_range[symbol][_][1] -
                                p.fingerprints_range[symbol][_][0]) \
                                > (10.**(-8.)):
                            scaled_value = -1. + \
                                2. * (indexfp[_] - p.fingerprints_range[
                                    symbol][_][0]) / \
                                (p.fingerprints_range[symbol][_][1] -
                                 p.fingerprints_range[symbol][_][0])
                        else:
                            scaled_value = indexfp[_]
                        scaled_indexfp[count] = scaled_value
                        count += 1
                    _, atomic_nn_energy = \
                        ff.get_output(scaled_indexfp, symbol)
                    self.energy += atomic_nn_energy
            self.results['energy'] = float(self.energy)

    ##################################################################

        if properties == ['forces']:

            if self.fortran and fmodules:
                send_image_force_data_to_f90_code(atoms, cf, self.elements,
                                                  p.hiddenlayers,
                                                  p.fingerprints_range)
                ravel = RavelVariables(p.weights, p.scalings)
                variables = ravel.to_vector(p.weights, p.scalings)
                self.forces = fmodules.get_forces(
                    no_of_atoms_=len(atoms),
                    variables=variables,
                    len_of_variables=len(variables),
                    activation=p.activation)
            else:

                # Neighborlists for all atoms are calculated.
                dict_nl = {}
                n_self_offsets = {}
                for self_atom in atoms:
                    self_index = self_atom.index
                    neighbor_indices, neighbor_offsets = \
                        _nl.get_neighbors(self_index)
                    n_self_indices = np.append(self_index, neighbor_indices)
                    if len(neighbor_offsets) == 0:
                        _n_self_offsets = [[0, 0, 0]]
                    else:
                        _n_self_offsets = np.vstack(([[0, 0, 0]],
                                                     neighbor_offsets))
                    dict_nl[self_index] = n_self_indices
                    n_self_offsets[self_index] = _n_self_offsets

                outputs = {}
                for atom in atoms:
                    index = atom.index
                    symbol = atom.symbol
                    indexfp = cf.index_fingerprint(symbol, index)
                    # fingerprints are scaled to [-1, 1] range
                    scaled_indexfp = [None] * len(indexfp)
                    count = 0
                    for _ in range(len(indexfp)):
                        if (p.fingerprints_range[symbol][_][1] -
                                p.fingerprints_range[symbol][_][0]) \
                                > (10.**(-8.)):
                            scaled_value = -1. + 2. * (indexfp[_] -
                                                       p.fingerprints_range[
                                symbol][_][0]) / \
                                (p.fingerprints_range[symbol][_][1] -
                                 p.fingerprints_range[symbol][_][0])
                        else:
                            scaled_value = indexfp[_]
                        scaled_indexfp[count] = scaled_value
                        count += 1
                    outputs[index], _ = ff.get_output(scaled_indexfp, symbol)

                self.forces[:] = 0.0

                for atom in atoms:
                    self_index = atom.index
                    n_self_indices = dict_nl[self_index]
                    _n_self_offsets = n_self_offsets[self_index]
                    n_self_symbols = [atoms[n_index].symbol
                                      for n_index in n_self_indices]
                    for ii in range(3):
                        force = 0.
                        for n_symbol, n_index, n_offset in zip(n_self_symbols,
                                                               n_self_indices,
                                                               _n_self_offsets,
                                                               ):
                            # for calculating forces, summation runs over
                            # neighbor atoms of type II (within the main cell
                            # only)
                            if n_offset[0] == 0 and n_offset[1] == 0 and \
                                    n_offset[2] == 0:
                                der_indexfp = \
                                    cf.get_der_fingerprint(n_symbol, n_index,
                                                           self_index, ii)
                                # fingerprint derivatives are scaled
                                scaled_der_indexfp = [None] * len(der_indexfp)
                                count = 0
                                for _ in range(len(der_indexfp)):
                                    if (p.fingerprints_range[n_symbol][_][1] -
                                            p.fingerprints_range[
                                            n_symbol][_][0]) \
                                            > (10.**(-8.)):
                                        scaled_value = 2. * der_indexfp[_] / \
                                            (p.fingerprints_range[
                                                n_symbol][_][1] -
                                             p.fingerprints_range[
                                                n_symbol][_][0])
                                    else:
                                        scaled_value = der_indexfp[_]
                                    scaled_der_indexfp[count] = scaled_value
                                    count += 1

                                force += ff.get_force(scaled_der_indexfp,
                                                      n_symbol,
                                                      outputs[n_index])

                        self.forces[self_index][ii] = force

                del dict_nl, outputs, n_self_offsets, n_self_indices,
                n_self_symbols, _n_self_offsets, scaled_indexfp, indexfp

            self.results['forces'] = self.forces

    #########################################################################

    def train(
            self,
            images,
            energy_goal=0.001,
            force_goal=0.005,
            overfitting_constraint=0.,
            force_coefficient=None,
            cores=None,
            optimizer=optimizer,
            read_fingerprints=True,
            overwrite=False,):
        """Fits a variable set to the data, by default using the "fmin_bfgs"
        optimizer. The optimizer takes as input a cost function to reduce and
        an initial guess of variables and returns an optimized variable set.
        Inputs:
            images: list of ASE atoms objects
                    List of objects with positions, symbols, energies, and
                    forces in ASE format. This is the training set of data.
                    This can also be the path to an ASE trajectory (.traj)
                    or database (.db) file.  Energies can be obtained from
                    any reference, e.g. DFT calculations.
            energy_goal: threshold rms(energy per atom) error at which
                    simulation is converged.
                    The default value is in unit of eV.
            force_goal: threshold rmse per atom for forces at which simulation
                    is converged.  The default value is in unit of eV/Ang. If
                    'force_goal = None', forces will not be trained.
            overfitting_constraint: the constant to suppress overfitting.
                    A proper value for this constant is subtle and depends on
                    the train data set; a small value may not cure the
                    over-fitting issue, whereas a large value may cause
                    over-smoothness.
            force_coefficient: multiplier of force square error in
                    constructing the cost function. This controls how
                    significant force-fit is as compared to energy fit in
                    approaching convergence. It depends on force and energy
                    units. If not specified, guesses the value such that
                    energy and force contribute equally to the cost
                    function when they hit their converged values.
            cores: (int) number of cores to parallelize over
                    If not specified, attempts to determine from environment.
            optimizer: function
                    The optimization object. The default is to use scipy's
                    fmin_bfgs, but any optimizer that behaves in the same
                    way will do.
            read_fingerprints: (bool)
                    Determines whether or not the code should read fingerprints
                    already calculated and saved in the script directory.
            overwrite: (bool)
                    If a trained output file with the same name exists,
                    overwrite it.
        """
        p = self.parameters
        filename = make_filename(self.label, 'trained-parameters.json')
        if (not overwrite) and os.path.exists(filename):
            raise IOError('File exists: %s.\nIf you want to overwrite,'
                          ' set overwrite=True or manually delete.'
                          % filename)

        self.overfitting_constraint = overfitting_constraint

        if force_goal is None:
            train_forces = False
            if not force_coefficient:
                force_coefficient = 0.
        else:
            train_forces = True
            if not force_coefficient:
                force_coefficient = (energy_goal / force_goal)**2

        log = Logger(make_filename(self.label, 'train-log.txt'))

        log('Neural: BP training started. ' + now())
        self.cores = cores
        if not self.cores:
            self.cores = mp.cpu_count()
        log('Parallel processing over %i cores.\n' % self.cores)

        if isinstance(images, str):
            extension = os.path.splitext(images)[1]
            if extension == '.traj':
                images = io.Trajectory(images, 'r')
            elif extension == '.db':
                images = io.read(images)

        log('Training on %i images.' % len(images))

        # Images is converted to dictionary form; key is hash of image.
        log.tic()
        log('Hashing images...')
        dict_images = {}
        for image in images:
            key = hash_image(image)
            if key in dict_images.keys():
                log('Warning: Duplicate image (based on identical hash).'
                    ' Was this expected? Hash: %s' % key)
            dict_images[key] = image
        images = dict_images.copy()
        del dict_images
        hash_keys = sorted(images.keys())
        log(' %i unique images after hashing.' % len(hash_keys))
        log(' ...hashing completed.', toc=True)

        # If hiddenlayers is not given, list of elements and hidden
        # layers are now generated. Also if hiddenlayers is fed by the user in
        # the tuple format, it will now be converted to a dictionary.
        if isinstance(p.hiddenlayers, tuple):

            self.elements = set([atom.symbol for hash_key in hash_keys
                                 for atom in images[hash_key]])
            self.elements = sorted(self.elements)

            hiddenlayers = {}
            for element in self.elements:
                hiddenlayers[element] = p.hiddenlayers
            self.set(hiddenlayers=hiddenlayers)

        msg = '%i unique elements included:' % len(self.elements)
        msg += '  ' + ', '.join(p.hiddenlayers.keys())
        log(msg)
        log('Hidden layer structure:')
        for item in p.hiddenlayers.items():
            log(' %2s: %s' % item)

        # If Gs is not given, generates symmetry functions
        if not p.Gs:
            self.set(Gs=make_symmetry_functions(self.elements))
        log('Symmetry functions for each element:')
        for _ in p.Gs.keys():
            log(' %2s: %i' % (_, len(p.Gs[_])))

        # If weights are not given, generates random weights
        if not p.weights:
            log('Initializing with random weights.')
            self.set(weights=make_weight_matrices(p.Gs,
                                                  self.elements,
                                                  p.hiddenlayers,
                                                  p.activation))
        else:
            log('Initial weights already present.')
        # If scalings are not given, generates random scalings
        if not p.scalings:
            log('Initializing with random scalings.')
            self.set(scalings=make_scalings_matrices(self.elements,
                                                     images,
                                                     p.activation))
        else:
            log('Initial scalings already present.')

        # "MultiProcess" object is initialized
        _mp = MultiProcess(self.fortran, no_procs=self.cores)

        # Neighborlist for all images are calculated and saved
        log.tic()
        snl = SaveNeighborLists(p.cutoff, hash_keys, images, self.dblabel,
                                log, train_forces, read_fingerprints)

        gc.collect()

        # Fingerprints are calculated and saved
        self.sfp = SaveFingerprints(
            p.cutoff,
            self.elements,
            p.Gs,
            hash_keys,
            images,
            self.dblabel,
            train_forces,
            read_fingerprints,
            snl,
            log,
            _mp)

        gc.collect()

        # If fingerprints_range has not been loaded, it will take value from
        # the json file.
        if p.fingerprints_range is None:
            p.fingerprints_range = self.sfp.fingerprints_range

        # If you want to use only one core for feed-forward evaluation
        # uncomment this line: Re-initializing "Multiprocessing" object with
        # one core
#        _mp = MultiProcess(self.fortran, no_procs=1)

        ravel = RavelVariables(p.weights, p.scalings)

        costfxn = CostFxnandDer(
            p.cutoff,
            p.Gs,
            p.hiddenlayers,
            ravel,
            hash_keys,
            images,
            p.activation,
            self.label,
            log,
            energy_goal,
            force_goal,
            self.sfp,
            p.fingerprints_range,
            snl,
            train_forces,
            _mp,
            p.fingerprints_tag,
            self.overfitting_constraint,
            force_coefficient,
            self.fortran)

        del hash_keys, images

        # saving initial parameters
        filename = make_filename(self.label, 'initial-parameters.json')
        save_parameters(filename, p.cutoff, p.activation, p.hiddenlayers,
                        p.Gs, p.fingerprints_tag, p.fingerprints_range,
                        p.weights, p.scalings)
        log('Initial parameters saved in file %s.' % filename)

        log('Starting optimization of cost function...')
        log(' Energy goal: %.3e' % energy_goal)
        if train_forces:
            log(' Force goal: %.3e' % force_goal)
            log(' Cost function force coefficient: %f' % force_coefficient)
        else:
            log(' No force training.')
        log.tic()
        converged = False

        variables = ravel.to_vector(p.weights, p.scalings)

        try:
            optimizer(f=costfxn.f, x0=variables, fprime=costfxn.fprime,
                      gtol=10. ** -500.)

        except ConvergenceOccurred:
            converged = True

        if not converged:
            log('Saving checkpoint data.')
            filename = make_filename(self.label, 'parameters-checkpoint.json')
            save_parameters(filename, p.cutoff, p.activation, p.hiddenlayers,
                            p.Gs, p.fingerprints_tag, p.fingerprints_range,
                            costfxn.weights, costfxn.scalings)
            log(' ...could not find parameters for the desired goal\n'
                'error. Least error parameters saved as checkpoint.\n'
                'Try it again or assign a larger value for "goal".',
                toc=True)
            raise TrainingConvergenceError()

        p.weights = costfxn.weights
        p.scalings = costfxn.scalings
        log(' ...optimization completed successfully. Optimal '
            'parameters saved.', toc=True)
        filename = make_filename(self.label, 'trained-parameters.json')
        save_parameters(filename, p.cutoff, p.activation, p.hiddenlayers,
                        p.Gs, p.fingerprints_tag, p.fingerprints_range,
                        p.weights, p.scalings)

        self.cost_function = costfxn.cost_function,
        self.energy_per_atom_rmse = costfxn.energy_per_atom_rmse
        self.force_rmse = costfxn.force_rmse
        self.der_variables_cost_function = costfxn.der_variables_square_error

##############################################################################
##############################################################################
##############################################################################


class FeedForward:

    """
    Class that implements a basic feed-forward neural network.
    Task:  Constructs the feed-forward neural
            network and calculates the output (energy).
    Inputs:
        hiddenlayers: dictionary of chemical element symbols and architectures
                    of their corresponding hidden layers of the conventional
                    neural network. Note that for each atom, number of nodes in
                    the input layer is always equal to the number of symmetry
                    functions (G), and the number of nodes in the output
                    layer is always one. For example,
                    hiddenlayers = {"O":(3,5), "Au":(5,6)} means that for "O"
                    we have two hidden layers, the first one with three nodes
                    and the second one having five nodes.
       weights: dictionary of symbols and dictionaries of arrays of weights;
                   each symbol and related dictionary corresponds to one
                   element and one conventional neural network.  The weights
                   dictionary for the above example has dimensions
                   weights = {"O": {1: np.array(149,3), 2: np.array(4,5),
                   3: np.array(6,1)}, "Au": {1: np.array(148,5),
                   2: np.array(6,6), 3: np.array(7,1)}. The arrays are set up
                   to connect node i in the previous layer with node j in the
                   current layer with indices w[i,j]. There are n+1 rows
                   (i values), with the last row being the bias, and m columns
                   (j values). If weights is not given, the arrays will be
                   randomly generated from values between -0.5 and 0.5.
        scalings: dictionary of variables for slope and intercept for each
                    element. They are used in order to remove the final value
                    from the range that the activation function would
                    otherwise be locked in. For example,
                      scalings={"Pd":{"slope":2, "intercept":1},
                                "O":{"slope":3, "intercept":4}}
        activation: string to assign the type of activation funtion. "linear"
                    refers to linear function, "tanh" refers to tanh function,
                    and "sigmoid" refers to sigmoid function.
        fortran: boolean
            If True, will use the fortran subroutines, else won't.


    **NOTE: The dimensions of the weight matrix should be consistent with
            hiddenlayers.**
    """

    def __init__(self, hiddenlayers, weights, scalings, activation, fortran):

        self.hiddenlayers = hiddenlayers
        self.weights = weights
        self.scalings = scalings
        self.activation = activation
        self.fortran = fortran
        elements = sorted(hiddenlayers.keys())
        self.hiddensizes = {}
        for element in elements:
            structure = hiddenlayers[element]
            if isinstance(structure, str):
                structure = structure.split('-')
            elif isinstance(structure, int):
                structure = [structure]
            else:
                structure = list(structure)
            hiddensizes = [int(part) for part in structure]
            self.hiddensizes[element] = hiddensizes
        del elements

    #########################################################################

    def get_output(self, indexfp, symbol):
        """Given fingerprints of the indexed atom and its symbol, outputs of
        the neural network are calculated."""

        hiddensizes = self.hiddensizes[symbol]
        weight = self.weights[symbol]
        o = {}  # node values
        layer = 1  # input layer
        net = {}  # excitation
        ohat = {}
        temp = np.zeros((1, len(indexfp) + 1))
        for i in range(len(indexfp)):
            temp[0, i] = indexfp[i]
        temp[0, len(indexfp)] = 1.0
        ohat[0] = temp
        net[1] = np.dot(ohat[0], weight[1])
        if self.activation == 'linear':
            o[1] = net[1]  # linear activation
        elif self.activation == 'tanh':
            o[1] = np.tanh(net[1])  # tanh activation
        elif self.activation == 'sigmoid':  # sigmoid activation
            o[1] = 1. / (1. + np.exp(-net[1]))
        temp = np.zeros((1, np.shape(o[1])[1] + 1))
        for i in range(np.shape(o[1])[1]):
            temp[0, i] = o[1][0, i]
        temp[0, np.shape(o[1])[1]] = 1.0
        ohat[1] = temp
        for hiddensize in hiddensizes[1:]:
            layer += 1
            net[layer] = np.dot(ohat[layer - 1], weight[layer])
            if self.activation == 'linear':
                o[layer] = net[layer]  # linear activation
            elif self.activation == 'tanh':
                o[layer] = np.tanh(net[layer])  # tanh activation
            elif self.activation == 'sigmoid':
                # sigmoid activation
                o[layer] = 1. / (1. + np.exp(-net[layer]))
            temp = np.zeros((1, np.size(o[layer]) + 1))
            for i in range(np.size(o[layer])):
                temp[0, i] = o[layer][0, i]
            temp[0, np.size(o[layer])] = 1.0
            ohat[layer] = temp
        layer += 1  # output layer
        net[layer] = np.dot(ohat[layer - 1], weight[layer])
        if self.activation == 'linear':
            o[layer] = net[layer]  # linear activation
        elif self.activation == 'tanh':
            o[layer] = np.tanh(net[layer])  # tanh activation
        elif self.activation == 'sigmoid':
            # sigmoid activation
            o[layer] = 1. / (1. + np.exp(-net[layer]))

        del hiddensizes, weight, ohat, net

        atomic_nn_energy = self.scalings[symbol]['slope'] * float(o[layer]) + \
            self.scalings[symbol]['intercept']

        # neural network outputs, atomic neural network energy
        return o, atomic_nn_energy

    #########################################################################

    def get_force(self, der_indexfp, n_symbol, outputs):
        """Feed-forward for calculating forces."""

        hiddensizes = self.hiddensizes[n_symbol]
        weight = self.weights[n_symbol]
        der_o = {}  # node values
        der_o[0] = der_indexfp
        layer = 0  # input layer
        for hiddensize in hiddensizes[0:]:
            layer += 1
            temp = np.dot(np.matrix(der_o[layer - 1]),
                          np.delete(weight[layer], -1, 0))
            der_o[layer] = [None] * np.size(outputs[layer])
            count = 0
            for j in range(np.size(outputs[layer])):
                if self.activation == 'linear':  # linear function
                    der_o[layer][count] = float(temp[0, j])
                elif self.activation == 'sigmoid':  # sigmoid function
                    der_o[layer][count] = float(outputs[layer][0, j] *
                                                (1. - outputs[layer][0, j])) \
                        * float(temp[0, j])
                elif self.activation == 'tanh':  # tanh function
                    der_o[layer][count] = float(1. - outputs[layer][0, j] *
                                                outputs[layer][0, j]) * \
                        float(temp[0, j])
                count += 1
        layer += 1  # output layer
        temp = np.dot(np.matrix(der_o[layer - 1]),
                      np.delete(weight[layer], -1, 0))
        if self.activation == 'linear':  # linear function
            der_o[layer] = float(temp)
        elif self.activation == 'sigmoid':  # sigmoid function
            der_o[layer] = float(outputs[layer] * (1. - outputs[layer]) * temp)
        elif self.activation == 'tanh':  # tanh function
            der_o[layer] = float((1. - outputs[layer] * outputs[layer]) * temp)

        force = float(-(self.scalings[n_symbol]['slope'] * der_o[layer]))

        return force

##############################################################################
##############################################################################
##############################################################################


class CalculateFingerprints:

    """
    Class that calculates fingerprints.
    Inputs:
        cutoff: float (default 6.5 Angstroms)
                  Radius above which neighbor interactions are ignored.
        Gs:  dictionary of symbols and lists of dictionaries for making
                    symmetry functions. Either auto-genetrated, or given
               in the following form:
                    Gs = {"O": [..., ...], "Au": [..., ...]} where
                    ... = {"type":"G2", "element":"O", "eta":0.0009} or
                    ... = {"type":"G4", "elements":["O", "Au"], "eta":0.0001,
                           "gamma":0.1, "zeta":1.0}
        atoms: ASE atoms object
                   The initial atoms object on which the fingerprints will be
                   generated.
        _nl: ASE NeighborList object
        fortran: boolean
            If True, will use the fortran subroutines, else won't.
    """

    def __init__(self, cutoff, Gs, atoms, _nl, fortran):
        self.cutoff = cutoff
        self.Gs = Gs
        self.atoms = atoms
        self._nl = _nl
        self.fortran = fortran

    #########################################################################

    def index_fingerprint(self, symbol, index):
        """Returns the fingerprint of symmetry function values for atom
        specified by its index. Will automatically update if atom positions
        has changed; you don't need to call update() unless you are
        specifying a new atoms object."""

        neighbor_indices, neighbor_offsets = self._nl.get_neighbors(index)
        # for calculating fingerprints, summation runs over neighboring atoms
        # of type I (either inside or outside the main cell)
        neighbor_symbols = [self.atoms[n_index].symbol
                            for n_index in neighbor_indices]
        Rs = [self.atoms.positions[n_index] +
              np.dot(n_offset, self.atoms.get_cell()) for n_index, n_offset
              in zip(neighbor_indices, neighbor_offsets)]
        fingerprint = [self.process_G(G, index, neighbor_symbols, Rs)
                       for G in self.Gs[symbol]]

        return fingerprint

    #########################################################################

    def get_der_fingerprint(self, symbol, index, mm, ii):
        """Returns the derivative of the fingerprint of symmetry function
        values for atom specified by its index with respect to coordinate
        x_{ii} of atom index mm."""

        Rindex = self.atoms.positions[index]
        neighbor_indices, neighbor_offsets = self._nl.get_neighbors(index)
        # for calculating derivatives of fingerprints, summation runs over
        # neighboring atoms of type I (either inside or outside the main cell)
        neighbor_symbols = [self.atoms[_index].symbol
                            for _index in neighbor_indices]
        Rs = [self.atoms.positions[_index] +
              np.dot(_offset, self.atoms.get_cell()) for _index, _offset
              in zip(neighbor_indices, neighbor_offsets)]
        der_fingerprint = [self.process_der_G(G,
                                              neighbor_indices,
                                              neighbor_symbols,
                                              Rs,
                                              index,
                                              Rindex,
                                              mm,
                                              ii) for G in self.Gs[symbol]]

        return der_fingerprint

    #########################################################################

    def process_G(self, G, index, symbols, Rs):
        """Returns the value of G for atom at index. symbols and Rs are
        lists of neighbors' symbols and Cartesian positions, respectively.
        """
        home = self.atoms[index].position

        if G['type'] == 'G2':
            return calculate_G2(symbols, Rs, G['element'], G['eta'],
                                self.cutoff, home, self.fortran)
        elif G['type'] == 'G4':
            return calculate_G4(symbols, Rs, G['elements'], G['gamma'],
                                G['zeta'], G['eta'], self.cutoff, home,
                                self.fortran)
        else:
            raise NotImplementedError('Unknown G type: %s' % G['type'])

    #########################################################################

    def process_der_G(self, G, indices, symbols, Rs, a, Ra, m, i):
        """Returns the value of the derivative of G for atom at index a and
        position Ra with respect to coordinate x_{i} of atom index m.
        Symbols and Rs are lists of neighbors' symbols and Cartesian positions,
        respectively.
        """

        if G['type'] == 'G2':
            return calculate_der_G2(indices, symbols, Rs, G['element'],
                                    G['eta'], self.cutoff, a, Ra, m, i,
                                    self.fortran)
        elif G['type'] == 'G4':
            return calculate_der_G4(
                indices,
                symbols,
                Rs,
                G['elements'],
                G['gamma'],
                G['zeta'],
                G['eta'],
                self.cutoff,
                a,
                Ra,
                m,
                i,
                self.fortran)
        else:
            raise NotImplementedError('Unknown G type: %s' % G['type'])


##############################################################################
##############################################################################
##############################################################################


class CostFxnandDer:

    """Cost function and its derivative based on squared difference in energy,
    to be optimized in setting variables.

        cutoff: cutoff radius, in Angstroms, around each atom

        Gs: fingerprint generator function descriptions

        hiddenlayers: structure of hidden layers, e.g., (5, 5)

        ravel: decoder to convert vector to biases/weights and vice versa

        hash_keys: unique keys for each of "images"

        images: list of ASE atoms objects (the training set)

        activation: the type of activation function

        label: name used for all files.

        log: write function at which to log data. Note this must be a
           callable function

        energy_goal: threshold rms(error per atom) at which simulation is
        converged

        force_goal: threshold rmse/atom at which simulation is converged

        sfp: object of the class SaveFingerprints, which contains all
           fingerprints

        fingerprints_range: range of fingerprints of each chemical species

        snl: object of the class SaveNeighborLists

        train_forces:  boolean representing whether forces are also trained or
        not

        _mp: object of "MultiProcess" class

        fingerprints_tag: a tag for identifying the functional form of
        fingerprints used in the code

        overfitting_constraint: the constant to constraint overfitting

        force_coefficient: multiplier of force RMSE in constructing the cost
        function. This controls how tight force-fit is as compared to
        energy fit. It also depends on force and energy units. Working with
        eV and Angstrom, 0.04 seems to be a reasonable value.

        fortran: boolean
            If True, will use the fortran subroutines, else won't.

    """
    #########################################################################

    def __init__(self, cutoff, Gs, hiddenlayers, ravel, hash_keys, images,
                 activation, label, log, energy_goal, force_goal, sfp,
                 fingerprints_range, snl, train_forces, _mp, fingerprints_tag,
                 overfitting_constraint, force_coefficient, fortran):
        self.cutoff = cutoff
        self.Gs = Gs
        self.hiddenlayers = hiddenlayers
        self.ravel = ravel
        self.hash_keys = hash_keys
        self.images = images
        self.activation = activation
        self.label = label
        self.log = log
        self.energy_goal = energy_goal
        self.force_goal = force_goal
        self.sfp = sfp
        self.fingerprints_range = fingerprints_range
        self.snl = snl
        self.train_forces = train_forces
        self.steps = 0
        self.elements = sorted(self.hiddenlayers.keys())
        self._mp = _mp
        self.fingerprints_tag = fingerprints_tag
        self.overfitting_constraint = overfitting_constraint
        self.force_coefficient = force_coefficient
        self.fortran = fortran

        self.energy_convergence = False
        self.force_convergence = False
        if not self.train_forces:
            self.force_convergence = True

        self.energy_coefficient = 1.0

        self.no_of_images = len(self.hash_keys)

        # all images are shared between cores for feed-forward and
        # back-propagation calculations
        self._mp.make_list_of_sub_images(self.hash_keys, self.images)

        if self.fortran:
            no_procs = self._mp.no_procs
            no_sub_images = [len(self._mp.list_sub_images[i])
                             for i in range(no_procs)]
            send_images_data_to_f90_code(self.hash_keys, self.images,
                                         self.sfp, self.snl,
                                         self.elements, self.hiddenlayers,
                                         self.train_forces,
                                         self.energy_coefficient,
                                         self.force_coefficient,
                                         log,
                                         self.fingerprints_range,
                                         no_procs,
                                         no_sub_images)
            _mp.initialize_manager()
            del self._mp.list_sub_images, self._mp.list_sub_hashes
        del self.hash_keys, hash_keys, self.images, images

        gc.collect()

    #########################################################################

    def f(self, variables):
        """function to calculate the cost function"""

        log = self.log
        self.weights, self.scalings = self.ravel.to_dicts(variables)
        ff = FeedForward(self.hiddenlayers, self.weights, self.scalings,
                         self.activation, self.fortran)

        if self.fortran:
            task_args = (ff,)
            (energy_square_error,
             force_square_error,
             self.der_variables_square_error) = \
                self._mp.share_cost_function_task_between_cores(
                task=_calculate_cost_function_fortran,
                _args=task_args, len_of_variables=len(variables))
        else:
            task_args = (ff, self.sfp, self.snl, self.energy_coefficient,
                         self.force_coefficient)
            (energy_square_error,
             force_square_error,
             self.der_variables_square_error) = \
                self._mp.share_cost_function_task_between_cores(
                task=_calculate_cost_function_python,
                _args=task_args, len_of_variables=len(variables))

        square_error = self.energy_coefficient * energy_square_error + \
            self.force_coefficient * force_square_error

        self.cost_function = square_error

        if self.overfitting_constraint > (10. ** (-12.)):
            weights_norm, self.der_of_weights_norm = \
                self.ravel.calculate_weights_norm_and_der(self.weights)
            self.cost_function += self.overfitting_constraint * weights_norm

        self.energy_per_atom_rmse = \
            np.sqrt(energy_square_error / self.no_of_images)
        self.force_rmse = np.sqrt(force_square_error / self.no_of_images)

        if self.steps == 0:
            if self.train_forces is True:
                head1 = ('%5s  %19s  %9s  %9s  %9s')
                log(head1 % ('', '', '',
                             ' (Energy',
                             ''))
                log(head1 % ('', '', 'Cost',
                             'per Atom)',
                             'Force'))
                log(head1 % ('Step', 'Time', 'Function',
                             'RMSE',
                             'RMSE'))
                log(head1 %
                    ('=' * 5, '=' * 19, '=' * 9, '=' * 9, '=' * 9))
            else:
                head1 = ('%3s  %16s %18s %10s')
                log(head1 % ('Step', 'Time', 'Cost',
                             '    RMS (Energy'))
                head2 = ('%3s  %28s %10s %12s')
                log(head2 % ('', '', 'Function',
                             'per Atom)'))
                head3 = ('%3s  %26s %10s %11s')
                log(head3 % ('', '', '',
                             '   Error'))
                log(head3 %
                    ('=' * 5, '=' * 26, '=' * 9, '=' * 10))

        self.steps += 1

        if self.train_forces is True:
            line = ('%5s' '  %19s' '  %9.3e' '  %9.3e' '  %9.3e')
            log(line % (self.steps - 1,
                        now(),
                        self.cost_function,
                        self.energy_per_atom_rmse,
                        self.force_rmse))
        else:
            line = ('%5s' ' %26s' ' %10.3e' ' %12.3e')
            log(line % (self.steps - 1,
                        now(),
                        self.cost_function,
                        self.energy_per_atom_rmse))

        if self.steps % 100 == 0:
            log('Saving checkpoint data.')
            filename = make_filename(
                self.label,
                'parameters-checkpoint.json')
            save_parameters(filename, self.cutoff, self.activation,
                            self.hiddenlayers, self.Gs,
                            self.fingerprints_tag,
                            self.fingerprints_range, self.weights,
                            self.scalings)

        if self.energy_per_atom_rmse < self.energy_goal and \
                self.energy_convergence is False:
            log('Energy convergence!')
            self.energy_convergence = True
        elif self.energy_per_atom_rmse > self.energy_goal and \
                self.energy_convergence is True:
            log('Energy unconverged!')
            self.energy_convergence = False

        if self.train_forces:
            if self.force_rmse < self.force_goal and \
                    self.force_convergence is False:
                log('Force convergence!')
                self.force_convergence = True
            elif self.force_rmse > self.force_goal and \
                    self.force_convergence is True:
                log('Force unconverged!')
                self.force_convergence = False

        if self.energy_convergence and self.force_convergence:
            self.weights, self.scalings = self.ravel.to_dicts(variables)
            raise ConvergenceOccurred

        gc.collect()

        return self.cost_function

    #########################################################################

    def fprime(self, variables):
        """function to calculate derivative of the cost function"""

        if self.steps == 0:

            self.weights, self.scalings = self.ravel.to_dicts(variables)
            ff = FeedForward(self.hiddenlayers, self.weights, self.scalings,
                             self.activation, self.fortran)

            if self.fortran:
                task_args = (ff,)
                (energy_square_error,
                 force_square_error,
                 self.der_variables_square_error) = \
                    self._mp.share_cost_function_task_between_cores(
                    task=_calculate_cost_function_fortran,
                    _args=task_args, len_of_variables=len(variables))
            else:
                task_args = (ff,
                             self.sfp,
                             self.snl,
                             self.energy_coefficient,
                             self.force_coefficient)
                (energy_square_error,
                 force_square_error,
                 self.der_variables_square_error) = \
                    self._mp.share_cost_function_task_between_cores(
                    task=_calculate_cost_function_python,
                    _args=task_args, len_of_variables=len(variables))

            if self.overfitting_constraint > (10. ** (-12.)):
                weights_norm, self.der_of_weights_norm = \
                    self.ravel.calculate_weights_norm_and_der(self.weights)

        der_cost_function = self.der_variables_square_error
        if self.overfitting_constraint > (10. ** (-12.)):
            for i in range(len(self.der_variables_square_error)):
                der_cost_function[i] += self.overfitting_constraint * \
                    self.der_of_weights_norm[i]

        der_cost_function = np.array(der_cost_function)

        return der_cost_function

##############################################################################
##############################################################################
##############################################################################


class ConvergenceOccurred(Exception):

    """Kludge to decide when scipy's optimizers are complete."""
    pass

##############################################################################
##############################################################################
##############################################################################


class SaveNeighborLists:

    """Neighborlists for all images with the given cutoff value are calculated
    and saved. As well as neighboring atomic indices, neighboring atomic
    offsets from the main cell are also saved. Offsets become important when
    dealing with periodic systems. Neighborlists are generally of two types:
    Type I which consists of atoms within the cutoff distance either in the
    main cell or in the adjacent cells, and Type II which consists of atoms in
    the main cell only and within the cutoff distance.

        cutoff: cutoff radius, in Angstroms, around each atom

        hash_keys: unique keys for each of "images"

        images: list of ASE atoms objects (the training set)

        label: name used for all files.

        log: write function at which to log data. Note this must be a
           callable function

        train_forces:  boolean to representing whether forces are also trained
                        or not

        read_fingerprints: boolean to determines whether or not the code should
                            read fingerprints already calculated and saved in
                            the script directory.

    """

    ##########################################################################

    def __init__(self, cutoff, hash_keys, images, label, log, train_forces,
                 read_fingerprints):

        self.cutoff = cutoff
        self.images = images
        self.nl_data = {}

        if train_forces is True:
            log('Calculating neighborlist for each atom...')
            log.tic()
            if read_fingerprints:
                try:
                    filename = make_filename(label, 'neighborlists.json')
                    fp = paropen(filename, 'rb')
                    data = json.load(fp)
                except IOError:
                    log(' No saved neighborlist file found.')
                else:
                    for key1 in data.keys():
                        for key2 in data[key1]:
                            nl_value = data[key1][key2]
                            self.nl_data[(key1, int(key2))] = \
                                ([value[0] for value in nl_value],
                                 [value[1] for value in nl_value],)
                    log(' Saved neighborlist file %s loaded with %i entries.'
                        % (filename, len(data.keys())))
            else:
                log(' Neighborlists in the main directory will not be used.')

            new_images = {}
            old_hashes = set([key[0] for key in self.nl_data.keys()])
            for hash_key in hash_keys:
                if hash_key not in old_hashes:
                    new_images[hash_key] = images[hash_key]

            log(' Calculating %i of %i neighborlists.'
                % (len(new_images), len(images)))

            if len(new_images) != 0:
                new_hashs = sorted(new_images.keys())
                for hash_key in new_hashs:
                    image = new_images[hash_key]
                    nl = NeighborList(cutoffs=([self.cutoff / 2.] *
                                               len(image)),
                                      self_interaction=False,
                                      bothways=True, skin=0.)
                    # FIXME: Is update necessary?
                    nl.update(image)
                    for self_atom in image:
                        self_index = self_atom.index
                        neighbor_indices, neighbor_offsets = \
                            nl.get_neighbors(self_index)
                        if len(neighbor_offsets) == 0:
                            n_self_offsets = [[0, 0, 0]]
                        else:
                            n_self_offsets = \
                                np.vstack(([[0, 0, 0]], neighbor_offsets))
                        n_self_indices = np.append(self_index,
                                                   neighbor_indices)
                        self.nl_data[(hash_key, self_index)] = \
                            (n_self_indices, n_self_offsets,)

                dict_data = OrderedDict()
                key0s = set([key[0] for key in self.nl_data.keys()])
                for key0 in key0s:
                    dict_data[key0] = {}
                for key in self.nl_data.keys():
                    dict_data[key[0]][key[1]] = self.nl_data[key]
                filename = make_filename(label, 'neighborlists.json')
                save_neighborlists(filename, dict_data)
                log(' ...neighborlists calculated and saved to %s.' %
                    filename, toc=True)

                del new_hashs, dict_data
            del new_images, old_hashes
        del images, self.images

##############################################################################
##############################################################################
##############################################################################


class SaveFingerprints:

    """Memory class to not recalculate fingerprints and their derivatives if
    not necessary. This could cause runaway memory usage; use with caution.

        cutoff: cutoff radius, in Angstroms, around each atom

        elements: list if elements in images

        Gs: fingerprint generator function descriptions

        hash_keys: unique keys for each of "images"

        images: list of ASE atoms objects (the training set)

        label: name used for all files

        train_forces:  boolean representing whether forces are also trained or
                        not

        read_fingerprints: boolean to determines whether or not the code should
                            read fingerprints already calculated and saved in
                            the script directory.

        snl: object of the class SaveNeighborLists

        log: write function at which to log data. Note this must be a
           callable function

        _mp: "MultiProcess" object

    """

    ##########################################################################

    def __init__(self,
                 cutoff,
                 elements,
                 Gs,
                 hash_keys,
                 images,
                 label,
                 train_forces,
                 read_fingerprints,
                 snl,
                 log,
                 _mp):

        self.Gs = Gs
        self.train_forces = train_forces
        self.fp_data = {}
        self.der_fp_data = {}

        log('Calculating atomic fingerprints...')
        log.tic()

        if read_fingerprints:
            try:
                filename = make_filename(label, 'fingerprints.json')
                fp = paropen(filename, 'rb')
                data = json.load(fp)
            except IOError:
                log('No saved fingerprint file found.')
            else:
                log.tic('read_fps')
                log(' Reading fingerprints from %s...' % filename)
                for key1 in data.keys():
                    for key2 in data[key1]:
                        fp_value = data[key1][key2]
                        self.fp_data[(key1, int(key2))] = \
                            [float(value) for value in fp_value]
                log(' ...fingerprints read',
                    toc='read_fps')
        else:
            log(' Pre-calculated fingerprints will not be used.')

        new_images = {}
        old_hashes = set([key[0] for key in self.fp_data.keys()])
        for hash_key in hash_keys:
            if hash_key not in old_hashes:
                new_images[hash_key] = images[hash_key]

        log(' Calculating %i of %i fingerprints. (%i exist in file.)'
            % (len(new_images), len(images), len(old_hashes)))

        if len(new_images) != 0:
            log.tic('calculate_fps')
            new_hashs = sorted(new_images.keys())
            # new images are shared between cores for fingerprint calculations
            _mp.make_list_of_sub_images(new_hashs, new_images)

            # Temporary files to hold child fingerprint calculations.
            childfiles = [tempfile.NamedTemporaryFile(prefix='fp-',
                                                      suffix='.json')
                          for _ in range(_mp.no_procs)]
            for _ in range(_mp.no_procs):
                log('  Processor %i calculations stored in file %s.'
                    % (_, childfiles[_].name))

            task_args = (cutoff, Gs, label,)
            _mp.share_fingerprints_task_between_cores(
                task=_calculate_fingerprints, _args=task_args, dirs=childfiles)

            log(' Calculated %i new images.' % len(new_images),
                toc='calculate_fps')
            log.tic('read_fps')
            log(' Reading calculated child-fingerprints...')
            for f in childfiles:
                f.seek(0)
                data = json.load(f)
                for key1 in data.keys():
                    for key2 in data[key1]:
                        fp_value = data[key1][key2]
                        self.fp_data[(key1, int(key2))] = \
                            [float(value) for value in fp_value]
            log(' ...child-fingerprints are read.', toc='read_fps')

            del new_hashs, data

        dict_data = OrderedDict()
        keys1 = set([key[0] for key in self.fp_data.keys()])
        for key in keys1:
            dict_data[key] = {}
        for key in self.fp_data.keys():
            dict_data[key[0]][key[1]] = self.fp_data[key]
        if len(new_images) != 0:
            log.tic('save_fps')
            log(' Saving fingerprints...')
            filename = make_filename(label, 'fingerprints.json')
            save_fingerprints(filename, dict_data)
            log(' ...fingerprints saved to %s.' % filename,
                toc='save_fps')

        fingerprint_values = {}
        for element in elements:
            fingerprint_values[element] = {}
            for i in range(len(Gs[element])):
                fingerprint_values[element][i] = []

        for hash_key in hash_keys:
            image = images[hash_key]
            for atom in image:
                for i in range(len(Gs[atom.symbol])):
                    fingerprint_values[atom.symbol][i].append(
                        dict_data[hash_key][atom.index][i])

        fingerprints_range = OrderedDict()
        for element in elements:
            fingerprints_range[element] = \
                [[min(fingerprint_values[element][_]),
                  max(fingerprint_values[element][_])]
                 for _ in range(len(Gs[element]))]

        self.fingerprints_range = fingerprints_range

        del dict_data, keys1, new_images, old_hashes

        if train_forces is True:
            log('Calculating derivatives of atomic fingerprints '
                'with respect to coordinates...')
            log.tic('fp_forces')

            if read_fingerprints:
                try:
                    filename = make_filename(
                        label,
                        'fingerprint-derivatives.json')
                    fp = paropen(filename, 'rb')
                    data = json.load(fp)
                except IOError or ValueError:
                    log('Either no saved fingerprint-derivatives file found '
                        'or it cannot be read.')
                else:
                    log.tic('read_der_fps')
                    log(' Reading fingerprint derivatives from file %s' %
                        filename)
                    for key1 in data.keys():
                        for key2 in data[key1]:
                            key3 = json.loads(key2)
                            fp_value = data[key1][key2]
                            self.der_fp_data[(key1,
                                              (int(key3[0]),
                                               int(key3[1]),
                                               int(key3[2])))] = \
                                [float(value) for value in fp_value]
                    log(' ...fingerprint derivatives read.',
                        toc='read_der_fps')
            else:
                log(' Pre-calculated fingerprint derivatives will '
                    'not be used.')

            new_images = {}
            old_hashes = set([key[0] for key in self.der_fp_data.keys()])
            for hash_key in hash_keys:
                if hash_key not in old_hashes:
                    new_images[hash_key] = images[hash_key]

            log(' Calculating %i of %i fingerprint derivatives. '
                '(%i exist in file.)'
                % (len(new_images), len(images), len(old_hashes)))

            if len(new_images) != 0:
                log.tic('calculate_der_fps')
                new_hashs = sorted(new_images.keys())
                # new images are shared between cores for calculating
                # fingerprint derivatives
                _mp.make_list_of_sub_images(new_hashs, new_images)

                # Temporary files to hold child fingerprint calculations.
                childfiles = [tempfile.NamedTemporaryFile(prefix='fp-',
                                                          suffix='.json')
                              for _ in range(_mp.no_procs)]
                for _ in range(_mp.no_procs):
                    log('  Processor %i calculations stored in file %s.'
                        % (_, childfiles[_].name))

                task_args = (cutoff, Gs, snl, label,)
                _mp.share_fingerprints_task_between_cores(
                    task=_calculate_der_fingerprints,
                    _args=task_args,
                    dirs=childfiles,)

                log(' Calculated %i new images.' % len(new_images),
                    toc='calculate_der_fps')

                log.tic('read_der_fps')
                log(' Reading child-fingerprint-derivatives...')
                for f in childfiles:
                    f.seek(0)
                    data = json.load(f)
                    for key1 in data.keys():
                        for key2 in data[key1]:
                            key3 = json.loads(key2)
                            fp_value = data[key1][key2]
                            self.der_fp_data[(key1,
                                              (int(key3[0]),
                                               int(key3[1]),
                                               int(key3[2])))] = \
                                [float(value) for value in fp_value]
                log(' ...child-fingerprint-derivatives are read.',
                    toc='read_der_fps')

                log.tic('save_der_fps')
                dict_data = OrderedDict()
                keys1 = set([key[0] for key in self.der_fp_data.keys()])
                for key in keys1:
                    dict_data[key] = {}
                for key in self.der_fp_data.keys():
                    dict_data[key[0]][key[1]] = self.der_fp_data[key]
                filename = make_filename(label, 'fingerprint-derivatives.json')
                save_der_fingerprints(filename, dict_data)
                log(' ...fingerprint derivatives calculated and saved to %s.'
                    % filename, toc='save_der_fps')

                del dict_data, keys1, new_hashs
            del new_images, old_hashes
        del images

        log(' ...all fingerprint operations complete.', toc=True)

##############################################################################
##############################################################################
##############################################################################


class RavelVariables:

    """Class to ravel and unravel variable values into a single vector.
    This is used for feeding into the optimizer. Feed in a list of
    dictionaries to initialize the shape of the transformation. Note no
    data is saved in the class; each time it is used it is passed either
    the dictionaries or vector. The dictionaries for initialization should
    be two levels deep.
        weights, scalings: variables to ravel and unravel
    """

    ##########################################################################

    def __init__(self, weights, scalings):

        self.count = 0
        self.weightskeys = []
        self.scalingskeys = []
        for key1 in sorted(weights.keys()):  # element
            for key2 in sorted(weights[key1].keys()):  # layer
                value = weights[key1][key2]
                self.weightskeys.append({'key1': key1,
                                         'key2': key2,
                                         'shape': np.array(value).shape,
                                         'size': np.array(value).size})
                self.count += np.array(weights[key1][key2]).size
        for key1 in sorted(scalings.keys()):  # element
            for key2 in sorted(scalings[key1].keys()):  # slope / intercept
                self.scalingskeys.append({'key1': key1,
                                          'key2': key2})
                self.count += 1
        self.vector = np.zeros(self.count)
        self.der_of_weights_norm = np.zeros(self.count)

    #########################################################################

    def to_vector(self, weights, scalings):
        """Puts the weights and scalings embedded dictionaries into a single
        vector and returns it. The dictionaries need to have the identical
        structure to those it was initialized with."""

        vector = np.zeros(self.count)
        count = 0
        for k in sorted(self.weightskeys):
            lweights = np.array(weights[k['key1']][k['key2']]).ravel()
            vector[count:(count + lweights.size)] = lweights
            count += lweights.size
        for k in sorted(self.scalingskeys):
            vector[count] = scalings[k['key1']][k['key2']]
            count += 1
        return vector

    #########################################################################

    def to_dicts(self, vector):
        """Puts the vector back into weights and scalings dictionaries of the
        form initialized. vector must have same length as the output of
        unravel."""

        assert len(vector) == self.count
        count = 0
        weights = OrderedDict()
        scalings = OrderedDict()
        for k in sorted(self.weightskeys):
            if k['key1'] not in weights.keys():
                weights[k['key1']] = OrderedDict()
            matrix = vector[count:count + k['size']]
            matrix = matrix.flatten()
            matrix = np.matrix(matrix.reshape(k['shape']))
            weights[k['key1']][k['key2']] = matrix
            count += k['size']
        for k in sorted(self.scalingskeys):
            if k['key1'] not in scalings.keys():
                scalings[k['key1']] = OrderedDict()
            scalings[k['key1']][k['key2']] = vector[count]
            count += 1
        return weights, scalings

    #########################################################################

    def calculate_weights_norm_and_der(self, weights):
        """Calculates the norm of weights as well as the vector of its
        derivative to constratint overfitting."""
        count = 0
        weights_norm = 0.
        for k in sorted(self.weightskeys):
            weight = weights[k['key1']][k['key2']]
            lweights = np.array(weight).ravel()
            # there is no overfitting constraint on the values of biases
            for i in range(np.shape(weight)[1]):
                lweights[- (i + 1)] = 0.
            for i in range(len(lweights)):
                weights_norm += lweights[i] ** 2.
            self.der_of_weights_norm[count:(count + lweights.size)] = \
                2. * lweights
            count += lweights.size
        for k in sorted(self.scalingskeys):
            # there is no overfitting constraint on the values of scalings
            self.der_of_weights_norm[count] = 0.
            count += 1
        return weights_norm, self.der_of_weights_norm

##############################################################################
##############################################################################
##############################################################################


def make_weight_matrices(Gs, elements, hiddenlayers, activation):
    """Makes random weight matrices from variables according to the convention
    used in Behler (J. Chem. Physc.: atom-centered symmetry functions for
    constructing ...).
    """
    if activation == 'linear':
        weight_range = 0.3
    else:
        weight_range = 3.

    weight = {}
    nn_structure = {}
    for element in sorted(elements):
        if isinstance(hiddenlayers[element], int):
            nn_structure[element] = ([len(Gs[element])] +
                                     [hiddenlayers[element]] +
                                     [1])
        else:
            nn_structure[element] = (
                [len(Gs[element])] +
                [layer for layer in hiddenlayers[element]] + [1])
        weight[element] = {}
        normalized_weight_range = weight_range / len(Gs[element])
        weight[element][1] = np.random.random((len(Gs[element]) + 1,
                                               nn_structure[element][1])) * \
            normalized_weight_range - \
            normalized_weight_range / 2.
        for layer in range(len(list(nn_structure[element])) - 3):
            normalized_weight_range = weight_range / \
                nn_structure[element][layer + 1]
            weight[element][layer + 2] = np.random.random(
                (nn_structure[element][layer + 1] + 1,
                 nn_structure[element][layer + 2])) * \
                normalized_weight_range - normalized_weight_range / 2.
        normalized_weight_range = weight_range / nn_structure[element][-2]
        weight[element][len(list(nn_structure[element])) - 1] = \
            np.random.random((nn_structure[element][-2] + 1, 1)) \
            * normalized_weight_range - normalized_weight_range / 2.
        for i in range(len(weight[element])):  # biases
            size = weight[element][i + 1][-1].size
            for jj in range(size):
                weight[element][i + 1][-1][jj] = 0.
    return weight

##############################################################################


def make_scalings_matrices(elements, images, activation):
    """Makes initial scaling matrices, such that the range of activation
    is scaled to the range of actual energies."""

    max_act_energy = max(image.get_potential_energy(apply_constraint=False)
                         for hash_key, image in images.items())
    min_act_energy = min(image.get_potential_energy(apply_constraint=False)
                         for hash_key, image in images.items())

    for hash_key, image in images.items():
        if image.get_potential_energy(apply_constraint=False) == \
                max_act_energy:
            no_atoms_of_max_act_energy = len(image)
        if image.get_potential_energy(apply_constraint=False) == \
                min_act_energy:
            no_atoms_of_min_act_energy = len(image)

    max_act_energy_per_atom = max_act_energy / no_atoms_of_max_act_energy
    min_act_energy_per_atom = min_act_energy / no_atoms_of_min_act_energy

    scaling = {}
    for element in elements:
        scaling[element] = {}
        if activation == 'sigmoid':  # sigmoid activation function
            scaling[element]['intercept'] = min_act_energy_per_atom
            scaling[element]['slope'] = (max_act_energy_per_atom -
                                         min_act_energy_per_atom)
        elif activation == 'tanh':  # tanh activation function
            scaling[element]['intercept'] = (max_act_energy_per_atom +
                                             min_act_energy_per_atom) / 2.
            scaling[element]['slope'] = (max_act_energy_per_atom -
                                         min_act_energy_per_atom) / 2.
        elif activation == 'linear':  # linear activation function
            scaling[element]['intercept'] = (max_act_energy_per_atom +
                                             min_act_energy_per_atom) / 2.
            scaling[element]['slope'] = (10. ** (-10.)) * \
                                        (max_act_energy_per_atom -
                                         min_act_energy_per_atom) / 2.
    del images

    return scaling

##############################################################################


def calculate_G2(symbols, Rs, G_element, eta, cutoff, home, fortran):
    """Calculate G2 symmetry function. Ideally this will not be used but
    will be a template for how to build the fortran version (and serves as
    a slow backup if the fortran one goes uncompiled)."""

    if fortran:  # fortran version; faster
        G_number = [atomic_numbers[G_element]]
        numbers = [atomic_numbers[symbol] for symbol in symbols]
        if len(Rs) == 0:
            ridge = 0.
        else:
            ridge = fmodules.calculate_g2(numbers=numbers, rs=Rs,
                                          g_number=G_number, g_eta=eta,
                                          cutoff=cutoff, home=home)
        return ridge
    else:
        ridge = 0.  # One aspect of a fingerprint :)
        for symbol, R in zip(symbols, Rs):
            if symbol == G_element:
                Rij = np.linalg.norm(R - home)
                ridge += (np.exp(-eta * (Rij ** 2.) / (cutoff ** 2.)) *
                          cutoff_fxn(Rij, cutoff))
        return ridge

##############################################################################


def calculate_G4(symbols, Rs, G_elements, gamma, zeta, eta, cutoff, home,
                 fortran):
    """Calculate G4 symmetry function. Ideally this will not be used but
    will be a template for how to build the fortran version (and serves as
    a slow backup if the fortran one goes uncompiled)."""

    if fortran:  # fortran version; faster
        G_numbers = sorted([atomic_numbers[el] for el in G_elements])
        numbers = [atomic_numbers[symbol] for symbol in symbols]
        if len(Rs) == 0:
            ridge = 0.
        else:
            ridge = fmodules.calculate_g4(numbers=numbers, rs=Rs,
                                          g_numbers=G_numbers, g_gamma=gamma,
                                          g_zeta=zeta, g_eta=eta,
                                          cutoff=cutoff, home=home)
        return ridge
    else:
        ridge = 0.
        counts = range(len(symbols))
        for j in counts:
            for k in counts[(j + 1):]:
                els = sorted([symbols[j], symbols[k]])
                if els != G_elements:
                    continue
                Rij_ = Rs[j] - home
                Rij = np.linalg.norm(Rij_)
                Rik_ = Rs[k] - home
                Rik = np.linalg.norm(Rik_)
                Rjk = np.linalg.norm(Rs[j] - Rs[k])
                cos_theta_ijk = np.dot(Rij_, Rik_) / Rij / Rik
                term = (1. + gamma * cos_theta_ijk) ** zeta
                term *= np.exp(-eta * (Rij ** 2. + Rik ** 2. + Rjk ** 2.) /
                               (cutoff ** 2.))
                term *= (1. / 3.) * (cutoff_fxn(Rij, cutoff) +
                                     cutoff_fxn(Rik, cutoff) +
                                     cutoff_fxn(Rjk, cutoff))
                ridge += term
        ridge *= 2. ** (1. - zeta)
        return ridge

##############################################################################


def make_symmetry_functions(elements):
    """Makes symmetry functions as in Nano Letters function by Artrith.
    Elements is a list of the elements, as in ["C", "O", "H", "Cu"].
    G[0] = {"type":"G2", "element": "O", "eta": 0.0009}
    G[40] = {"type":"G4", "elements": ["O", "Au"], "eta": 0.0001,
             "gamma": 1.0, "zeta": 1.0}
    If G (a list) is fed in, this will add to it and return an expanded
    version. If not, it will create a new one.
    """

    G = {}
    for element0 in elements:
        _G = []
        # Radial symmetry functions.
        etas = [0.05, 4., 20., 80.]
        for eta in etas:
            for element in elements:
                _G.append({'type': 'G2', 'element': element, 'eta': eta})
        # Angular symmetry functions.
        etas = [0.005]
        zetas = [1., 4.]
        gammas = [+1., -1.]
        for eta in etas:
            for zeta in zetas:
                for gamma in gammas:
                    for i1, el1 in enumerate(elements):
                        for el2 in elements[i1:]:
                            els = sorted([el1, el2])
                            _G.append({'type': 'G4',
                                       'elements': els,
                                       'eta': eta,
                                       'gamma': gamma,
                                       'zeta': zeta})
        G[element0] = _G
    return G

##############################################################################


def cutoff_fxn(Rij, Rc):
    """Cosine cutoff function in Parinello-Behler method."""
    if Rij > Rc:
        return 0.
    else:
        return 0.5 * (np.cos(np.pi * Rij / Rc) + 1.)

##############################################################################


def der_cutoff_fxn(Rij, Rc):
    """Derivative of the Cosine cutoff function."""
    if Rij > Rc:
        return 0.
    else:
        return -0.5 * np.pi / Rc * np.sin(np.pi * Rij / Rc)

##############################################################################


def Kronecker_delta(i, j):
    """Kronecker delta function."""
    if i == j:
        return 1.
    else:
        return 0.

##############################################################################


def der_position_vector(a, b, m, i):
    """Returns the derivative of the position vector R_{ab} with respect to
        x_{i} of atomic index m."""
    der_position_vector = [None, None, None]
    der_position_vector[0] = (Kronecker_delta(m, a) - Kronecker_delta(m, b)) \
        * Kronecker_delta(0, i)
    der_position_vector[1] = (Kronecker_delta(m, a) - Kronecker_delta(m, b)) \
        * Kronecker_delta(1, i)
    der_position_vector[2] = (Kronecker_delta(m, a) - Kronecker_delta(m, b)) \
        * Kronecker_delta(2, i)

    return der_position_vector

##############################################################################


def der_position(m, n, Rm, Rn, l, i):
    """Returns the derivative of the norm of position vector R_{mn} with
        respect to x_{i} of atomic index l."""
    Rmn = np.linalg.norm(Rm - Rn)
    # mm != nn is necessary for periodic systems
    if l == m and m != n:
        der_position = (Rm[i] - Rn[i]) / Rmn
    elif l == n and m != n:
        der_position = -(Rm[i] - Rn[i]) / Rmn
    else:
        der_position = 0.
    return der_position

##############################################################################


def der_cos_theta(a, j, k, Ra, Rj, Rk, m, i):
    """Returns the derivative of Cos(theta_{ajk}) with respect to
        x_{i} of atomic index m."""
    Raj_ = Ra - Rj
    Raj = np.linalg.norm(Raj_)
    Rak_ = Ra - Rk
    Rak = np.linalg.norm(Rak_)
    der_cos_theta = 1. / \
        (Raj * Rak) * np.dot(der_position_vector(a, j, m, i), Rak_)
    der_cos_theta += +1. / \
        (Raj * Rak) * np.dot(Raj_, der_position_vector(a, k, m, i))
    der_cos_theta += -1. / \
        ((Raj ** 2.) * Rak) * np.dot(Raj_, Rak_) * \
        der_position(a, j, Ra, Rj, m, i)
    der_cos_theta += -1. / \
        (Raj * (Rak ** 2.)) * np.dot(Raj_, Rak_) * \
        der_position(a, k, Ra, Rk, m, i)
    return der_cos_theta

##############################################################################


def calculate_der_G2(n_indices, symbols, Rs, G_element, eta, cutoff, a, Ra,
                     m, i, fortran):
    """Calculate coordinate derivative of G2 symmetry function for atom at
    index a and position Ra with respect to coordinate x_{i} of atom index
    m."""

    if fortran:  # fortran version; faster
        G_number = [atomic_numbers[G_element]]
        numbers = [atomic_numbers[symbol] for symbol in symbols]
        list_n_indices = [n_indices[_] for _ in range(len(n_indices))]
        if len(Rs) == 0:
            ridge = 0.
        else:
            ridge = fmodules.calculate_der_g2(n_indices=list_n_indices,
                                              numbers=numbers, rs=Rs,
                                              g_number=G_number,
                                              g_eta=eta, cutoff=cutoff,
                                              aa=a, home=Ra, mm=m,
                                              ii=i)
    else:
        ridge = 0.  # One aspect of a fingerprint :)
        for symbol, Rj, n_index in zip(symbols, Rs, n_indices):
            if symbol == G_element:
                Raj = np.linalg.norm(Ra - Rj)
                term1 = (-2. * eta * Raj * cutoff_fxn(Raj, cutoff) /
                         (cutoff ** 2.) +
                         der_cutoff_fxn(Raj, cutoff))
                term2 = der_position(a, n_index, Ra, Rj, m, i)
                ridge += np.exp(- eta * (Raj ** 2.) / (cutoff ** 2.)) * \
                    term1 * term2
    return ridge

##############################################################################


def calculate_der_G4(n_indices, symbols, Rs, G_elements, gamma, zeta, eta,
                     cutoff, a, Ra, m, i, fortran):
    """Calculate coordinate derivative of G4 symmetry function for atom at
    index a and position Ra with respect to coordinate x_{i} of atom index
    m."""

    if fortran:  # fortran version; faster
        G_numbers = sorted([atomic_numbers[el] for el in G_elements])
        numbers = [atomic_numbers[symbol] for symbol in symbols]
        list_n_indices = [n_indices[_] for _ in range(len(n_indices))]
        if len(Rs) == 0:
            ridge = 0.
        else:
            ridge = fmodules.calculate_der_g4(n_indices=list_n_indices,
                                              numbers=numbers, rs=Rs,
                                              g_numbers=G_numbers,
                                              g_gamma=gamma,
                                              g_zeta=zeta, g_eta=eta,
                                              cutoff=cutoff, aa=a,
                                              home=Ra, mm=m,
                                              ii=i)
    else:
        ridge = 0.
        counts = range(len(symbols))
        for j in counts:
            for k in counts[(j + 1):]:
                els = sorted([symbols[j], symbols[k]])
                if els != G_elements:
                    continue
                Rj = Rs[j]
                Rk = Rs[k]
                Raj_ = Rs[j] - Ra
                Raj = np.linalg.norm(Raj_)
                Rak_ = Rs[k] - Ra
                Rak = np.linalg.norm(Rak_)
                Rjk_ = Rs[j] - Rs[k]
                Rjk = np.linalg.norm(Rjk_)
                cos_theta_ajk = np.dot(Raj_, Rak_) / Raj / Rak
                c1 = (1. + gamma * cos_theta_ajk)
                c2 = cutoff_fxn(Raj, cutoff)
                c3 = cutoff_fxn(Rak, cutoff)
                c4 = cutoff_fxn(Rjk, cutoff)
                if zeta == 1:
                    term1 = \
                        np.exp(- eta * (Raj ** 2. + Rak ** 2. + Rjk ** 2.) /
                               (cutoff ** 2.))
                else:
                    term1 = c1 ** (zeta - 1.) * \
                        np.exp(- eta * (Raj ** 2. + Rak ** 2. + Rjk ** 2.) /
                               (cutoff ** 2.))
                term2 = (1. / 3.) * (c2 + c3 + c4)
                term3 = der_cos_theta(a, n_indices[j], n_indices[k], Ra, Rj,
                                      Rk, m, i)
                term4 = gamma * zeta * term3
                term5 = der_position(a, n_indices[j], Ra, Rj, m, i)
                term4 += -2. * c1 * eta * Raj * term5 / (cutoff ** 2.)
                term6 = der_position(a, n_indices[k], Ra, Rk, m, i)
                term4 += -2. * c1 * eta * Rak * term6 / (cutoff ** 2.)
                term7 = der_position(n_indices[j], n_indices[k], Rj, Rk, m, i)
                term4 += -2. * c1 * eta * Rjk * term7 / (cutoff ** 2.)
                term2 = term2 * term4
                term8 = c1 * (1. / 3.) * der_cutoff_fxn(Raj, cutoff) * term5
                term9 = c1 * (1. / 3.) * der_cutoff_fxn(Rak, cutoff) * term6
                term10 = c1 * (1. / 3.) * der_cutoff_fxn(Rjk, cutoff) * term7
                term11 = term2 + term8 + term9 + term10
                term = term1 * term11
                ridge += term
        ridge *= 2. ** (1. - zeta)

    return ridge

##############################################################################


def save_neighborlists(filename, neighborlists):
    """Save neighborlists in json format."""

    new_dict = {}
    for key1 in neighborlists.keys():
        new_dict[key1] = {}
        for key2 in neighborlists[key1].keys():
            nl_value = neighborlists[key1][key2]
            new_dict[key1][key2] = [[nl_value[0][i],
                                     [j for j in nl_value[1][i]]]
                                    for i in range(len(nl_value[0]))]

    with paropen(filename, 'wb') as outfile:
        json.dump(new_dict, outfile)

##############################################################################


def save_fingerprints(f, fingerprints):
    """Save fingerprints in json format. f is either a file object or a
    path."""

    new_dict = {}
    for key1 in fingerprints.keys():
        new_dict[key1] = {}
        for key2 in fingerprints[key1].keys():
            fp_value = fingerprints[key1][key2]
            new_dict[key1][key2] = [value for value in fp_value]

    try:
        json.dump(new_dict, f)
        f.flush()
        return
    except AttributeError:
        with paropen(f, 'wb') as outfile:
            json.dump(new_dict, outfile)

##############################################################################


def save_der_fingerprints(f, der_fingerprints):
    """Save derivatives of fingerprints in json format. f is either a file
    object or a path."""

    new_dict = {}
    for key1 in der_fingerprints.keys():
        new_dict[key1] = {}
        for key2 in der_fingerprints[key1].keys():
            fp_value = der_fingerprints[key1][key2]
            new_dict[key1][str([key2[0], key2[1], key2[2]])] = [
                value for value in fp_value]

    try:
        json.dump(new_dict, f)
        f.flush()
        return
    except AttributeError:
        with paropen(f, 'wb') as outfile:
            json.dump(new_dict, outfile)

##############################################################################


def save_parameters(filename, cutoff, activation, hiddenlayers, Gs,
                    fingerprints_tag, fingerprints_range, weight,
                    scaling):
    """Save parameters in json format."""

    parameters = {}
    parameters['cutoff'] = cutoff
    parameters['activation'] = activation
    parameters['hiddenlayers'] = hiddenlayers
    parameters['Gs'] = Gs
    parameters['fingerprints_tag'] = fingerprints_tag
    parameters['fingerprints_range'] = fingerprints_range
    parameters['scalings'] = scaling

    # Reshaping weights
    reshaped_weights = OrderedDict()
    for key1 in sorted(weight.keys()):
        reshaped_weights[key1] = OrderedDict()
        dictionary = weight[key1]
        for key2 in sorted(dictionary.keys()):
            shape = np.shape(dictionary[key2])
            reshaped_weights[key1][key2] = []
            for i in range(shape[0]):
                reshaped_weights[key1][key2].append([])
                if isinstance(dictionary[key2][i], float):
                    value = dictionary[key2][i]
                    reshaped_weights[key1][key2][i].append(value)
                else:
                    for j in range(shape[1]):
                        if len(np.shape(dictionary[key2][i])) == 1:
                            value = float(dictionary[key2][i][j])
                            reshaped_weights[key1][key2][i].append(value)
                        else:
                            value = float(dictionary[key2][i][0, j])
                            reshaped_weights[key1][key2][i].append(value)

    parameters['weights'] = reshaped_weights

    base_filename = os.path.splitext(filename)[0]
    export_filename = os.path.join(base_filename + '.json')

    with paropen(export_filename, 'wb') as outfile:
        json.dump(parameters, outfile)

##############################################################################


def load_parameters(json_file):
    """Reads parameters from JSON file."""

    parameters = json.load(json_file)

    # Reshaping weights and scalings
    weights = parameters['weights']
    reshaped_weights = {}
    for key1 in sorted(weights.keys()):
        reshaped_weights[key1] = {}
        dictionary = weights[key1]
        for key2 in sorted(dictionary.keys()):
            shape = np.shape(dictionary[key2])
            reshaped_weights[key1][int(key2)] = []
            for i in range(shape[0]):
                reshaped_weights[key1][int(key2)].append([])
                for j in range(shape[1]):
                    value = float(dictionary[key2][i][j])
                    reshaped_weights[key1][int(key2)][i].append(value)
    scalings = parameters['scalings']
    reshaped_scalings = {}
    for element in sorted(scalings.keys()):
        reshaped_scalings[element] = {}
        reshaped_scalings[element]['intercept'] = \
            scalings[element]['intercept']
        reshaped_scalings[element]['slope'] = scalings[element]['slope']

    parameters['weights'] = reshaped_weights
    parameters['scalings'] = reshaped_scalings

    return parameters

##############################################################################


def make_filename(label, base_filename):
    """Creates a filename from the label and the base_filename which should be
    a string"""

    if not label:
        filename = base_filename
    else:
        filename = os.path.join(label + '-' + base_filename)

    return filename

##############################################################################
##############################################################################
##############################################################################


class MultiProcess:

    """Class to do parallel processing, using multiprocessing package which
    works on Python versions 2.6 and above.
    Inputs:
            fortran: boolean
                If True, will use the fortran subroutines, else won't.
            no_procs: number of processors
            """

    def __init__(self, fortran, no_procs):

        self.fortran = fortran
        self.no_procs = no_procs
        self.queues = {}
        for x in range(no_procs):
            self.queues[x] = mp.Queue()

    def initialize_manager(self,):

        manager = FmodulesManager()
        manager.start()
        self.fmodules_manager = manager.fmodules_()

    ##########################################################################

    def make_list_of_sub_images(self, hash_keys, images):
        """Two lists are made each with one entry per core. The entry of the
        first list contains list of hashes to be calculated by that core,
        and the entry of the second list contains dictionary of images to be
        calculated by that core."""

        self.hash_keys = hash_keys
        self.images = images

        quotient = int(len(hash_keys) / self.no_procs)
        remainder = len(hash_keys) - self.no_procs * quotient
        list_sub_hashes = [None] * self.no_procs
        list_sub_images = [None] * self.no_procs
        count0 = 0
        count1 = 0
        for i in range(self.no_procs):
            if i < remainder:
                len_sub_hashes = quotient + 1
            else:
                len_sub_hashes = quotient
            sub_hashes = [None] * len_sub_hashes
            sub_images = {}
            count2 = 0
            for j in range(len_sub_hashes):
                hash = hash_keys[count1]
                sub_hashes[count2] = hash
                sub_images[hash] = images[hash]
                count1 += 1
                count2 += 1
            list_sub_hashes[count0] = sub_hashes
            list_sub_images[count0] = sub_images
            count0 += 1

        self.list_sub_hashes = list_sub_hashes
        self.list_sub_images = list_sub_images

        del hash_keys, images, list_sub_hashes, list_sub_images, sub_hashes,
        sub_images

    ##########################################################################

    def share_fingerprints_task_between_cores(self, task, _args, dirs):
        """Fingerprints tasks are sent to cores for parallel processing"""

        args = {}
        for x in range(self.no_procs):
            sub_hash_keys = self.list_sub_hashes[x]
            sub_images = self.list_sub_images[x]
            args[x] = (sub_hash_keys, sub_images,) + _args + (dirs[x],) + \
                (self.fortran,)

        processes = [mp.Process(target=task, args=args[x])
                     for x in range(self.no_procs)]

        for x in range(self.no_procs):
            processes[x].start()

        for x in range(self.no_procs):
            processes[x].join()

        for x in range(self.no_procs):
            processes[x].terminate()

        del sub_hash_keys, sub_images

    ##########################################################################

    def share_cost_function_task_between_cores(self, task, _args,
                                               len_of_variables):
        """Derivatives of the cost function with respect to variables are
        calculated in parallel"""

        args = {}
        for x in range(self.no_procs):
            if self.fortran:
                args[x] = (x + 1,) + _args + (self.queues[x],) + \
                    (self.fmodules_manager,)
            else:
                sub_hash_keys = self.list_sub_hashes[x]
                sub_images = self.list_sub_images[x]
                args[x] = (sub_hash_keys, sub_images,) + _args + \
                    (self.queues[x],)

        energy_square_error = 0.
        force_square_error = 0.

        der_variables_square_error = [0.] * len_of_variables

        processes = [mp.Process(target=task, args=args[x])
                     for x in range(self.no_procs)]

        for x in range(self.no_procs):
            processes[x].start()

        for x in range(self.no_procs):
            processes[x].join()

        for x in range(self.no_procs):
            processes[x].terminate()

#       Construct total square_error and derivative with respect to variables
#       from subprocesses
        results = {}
        for x in range(self.no_procs):
            results[x] = self.queues[x].get()

        sub_energy_square_error = [results[x][0] for x in range(self.no_procs)]
        sub_force_square_error = [results[x][1] for x in range(self.no_procs)]
        sub_der_variables_square_error = [results[x][2]
                                          for x in range(self.no_procs)]

        for i in range(len(sub_energy_square_error)):
            energy_square_error += sub_energy_square_error[i]
            force_square_error += sub_force_square_error[i]
            for j in range(len_of_variables):
                der_variables_square_error[j] += \
                    sub_der_variables_square_error[i][j]

        if not self.fortran:
            del sub_hash_keys, sub_images
        del task, _args, results, sub_energy_square_error,
        sub_force_square_error, sub_der_variables_square_error

        return (energy_square_error, force_square_error,
                der_variables_square_error)

##############################################################################
##############################################################################
##############################################################################


class ExtrapolateError(Exception):

    """Error class in the case of extrapolation"""
    pass

##############################################################################
##############################################################################
##############################################################################


class FingerprintsError(Exception):

    """Error class in case the functional form of fingerprints has
    changed"""
    pass

##############################################################################
##############################################################################
##############################################################################


class TrainingConvergenceError(Exception):

    """Error to be raise if training does not converge."""
    pass

##############################################################################
##############################################################################
##############################################################################


def _calculate_fingerprints(hashes,
                            images,
                            cutoff,
                            Gs,
                            label,
                            childfile,
                            fortran):
    """
    wrapper function to be used in multiprocessing for calculating
    fingerprints.

    **Note** This function will run on all cores and should be very optimized
    in terms of speed and memory usage.

    """

    fingerprints = {}
    for hash_key in hashes:
        fingerprints[hash_key] = {}
        atoms = images[hash_key]
        _nl = NeighborList(cutoffs=([cutoff / 2.] * len(atoms)),
                           self_interaction=False,
                           bothways=True,
                           skin=0.)
        _nl.update(atoms)
        cf = CalculateFingerprints(cutoff, Gs, atoms, _nl, fortran)
        for atom in atoms:
            index = atom.index
            symbol = atom.symbol
            indexfp = cf.index_fingerprint(symbol, index)
            fingerprints[hash_key][index] = indexfp

    save_fingerprints(f=childfile, fingerprints=fingerprints)

    if len(hashes) != 0:
        del hashes, images, fingerprints, atoms, _nl, cf, indexfp

##############################################################################


def _calculate_der_fingerprints(hashes, images, cutoff, Gs,
                                snl, label, childfile, fortran):
    """
    wrapper function to be used in multiprocessing for calculating
    derivatives of fingerprints.

    **Note** This function will run on all cores and should be very optimized
    in terms of speed and memory usage.

    """

    data = {}
    for hash_key in hashes:
        data[hash_key] = {}
        atoms = images[hash_key]
        _nl = NeighborList(cutoffs=([cutoff / 2.] * len(atoms)),
                           self_interaction=False,
                           bothways=True,
                           skin=0.)
        _nl.update(atoms)
        cf = CalculateFingerprints(cutoff, Gs, atoms, _nl, fortran)
        for self_atom in atoms:
            self_index = self_atom.index
            n_self_indices = snl.nl_data[(hash_key, self_index)][0]
            n_self_offsets = snl.nl_data[(hash_key, self_index)][1]
            n_symbols = [atoms[n_index].symbol for n_index in n_self_indices]
            for n_symbol, n_index, n_offset in zip(n_symbols, n_self_indices,
                                                   n_self_offsets):
                # derivative of fingerprints are needed only with respect to
                # coordinates of atoms of type II (within the main cell only)
                if n_offset[0] == 0 and n_offset[1] == 0 and n_offset[2] == 0:
                    for i in range(3):
                        der_indexfp = cf.get_der_fingerprint(
                            n_symbol,
                            n_index,
                            self_index,
                            i)
                        data[hash_key][(n_index, self_index, i)] = der_indexfp

    save_der_fingerprints(childfile, data)

    if len(hashes) != 0:
        del hashes, images, data, atoms, _nl, cf, n_self_indices,
        n_self_offsets, n_symbols, der_indexfp

##############################################################################


def _calculate_cost_function_fortran(proc_no, ff, queue, fmodules_manager):
    """
    wrapper function to be used in multiprocessing for calculating
    cost function and it's derivative with respect to variables.

    **Note** This function will run on all cores and should be very optimized
    in terms of speed and memory usage.

    """

    activation = ff.activation
    weights = ff.weights
    scalings = ff.scalings

    energy_square_error = 0.
    force_square_error = 0.

    ravel = RavelVariables(weights, scalings)
    variables = ravel.to_vector(weights, scalings)

    (energy_square_error,
     force_square_error,
     der_variables_square_error) = \
        fmodules_manager.run(proc=proc_no,
                             variables=variables,
                             len_of_variables=len(variables),
                             activation=activation)

    del proc_no, ff, fmodules_manager, activation, weights, scalings,
    ravel, variables

    queue.put([energy_square_error,
               force_square_error,
               der_variables_square_error])

##############################################################################


def _calculate_cost_function_python(hashes, images, ff, sfp, snl,
                                    energy_coefficient,
                                    force_coefficient, queue):
    """wrapper function to be used in multiprocessing for calculating
    cost function and it's derivative with respect to variables"""

    activation = ff.activation
    weights = ff.weights
    scalings = ff.scalings

    energy_square_error = 0.
    force_square_error = 0.

    elements = sorted(ff.hiddenlayers.keys())
    der_weights_square_error = {}
    for element in elements:
        der_weights_square_error[element] = {}
        for j in range(len(weights[element])):
            der_weights_square_error[element][j + 1] = \
                np.zeros(shape=np.shape(weights[element][j + 1]))

    der_scalings_square_error = {}
    for element in elements:
        der_scalings_square_error[element] = {}
        der_scalings_square_error[element]['intercept'] = 0.
        der_scalings_square_error[element]['slope'] = 0.

    W = {}
    for element in elements:
        weight = weights[element]
        W[element] = {}
        for j in range(len(weight)):
            W[element][j + 1] = np.delete(weight[j + 1], -1, 0)

    for hash_key in hashes:
        atoms = images[hash_key]
        real_energy = atoms.get_potential_energy(apply_constraint=False)
        real_forces = atoms.get_forces(apply_constraint=False)
        o = {}
        D = {}
        delta = {}
        ohat = {}

        nn_energy = 0.

        for atom in atoms:
            index = atom.index
            symbol = atom.symbol
            indexfp = sfp.fp_data[(hash_key, index)]
            # fingerprints are scaled to [-1, 1] range
            scaled_indexfp = [None] * len(indexfp)
            count = 0
            for p in range(len(indexfp)):
                if (sfp.fingerprints_range[symbol][p][1] -
                        sfp.fingerprints_range[symbol][p][0]) > (10.**(-8.)):
                    scaled_value = -1. + 2. * (indexfp[p] -
                                               sfp.fingerprints_range[
                                               symbol][p][0]) / \
                        (sfp.fingerprints_range[symbol][p][1] -
                         sfp.fingerprints_range[symbol][p][0])
                else:
                    scaled_value = indexfp[p]
                scaled_indexfp[count] = scaled_value
                count += 1
            o[index], atomic_nn_energy = ff.get_output(scaled_indexfp, symbol)
            temp = np.zeros((1, len(indexfp)))
            for i in range(len(indexfp)):
                temp[0, i] = scaled_indexfp[i]
            o[index][0] = temp
            nn_energy += atomic_nn_energy

        energy_square_error += (nn_energy - real_energy) ** 2. / \
            (len(atoms) ** 2.)

        for atom in atoms:
            index = atom.index
            symbol = atom.symbol
            N = len(o[index]) - 2
            D[index] = {}
            for k in range(1, N + 2):
                # calculating D matrix
                D[index][k] = \
                    np.zeros(shape=(np.size(o[index][k]),
                                    np.size(o[index][k])))
                for j in range(np.size(o[index][k])):
                    if activation == 'linear':  # linear
                        D[index][k][j, j] = 1.
                    elif activation == 'sigmoid':  # sigmoid
                        D[index][k][j, j] = \
                            float(o[index][k][0, j]) * \
                            float((1. - o[index][k][0, j]))
                    elif activation == 'tanh':  # tanh
                        D[index][k][j, j] = \
                            float(1. - o[index][k][0, j] *
                                  o[index][k][0, j])
            # Calculating delta
            delta[index] = {}
            # output layer
            delta[index][N + 1] = D[index][N + 1]
            # hidden layers
            for k in range(N, 0, -1):
                delta[index][k] = np.dot(D[index][k],
                                         np.dot(W[symbol][k + 1],
                                                delta[index][k + 1]))
            # Calculating ohat
            ohat[index] = {}
            for k in range(1, N + 2):
                ohat[index][k - 1] = \
                    np.zeros(shape=(1, np.size(o[index][k - 1]) + 1))
                for j in range(np.size(o[index][k - 1])):
                    ohat[index][k - 1][0, j] = o[index][k - 1][0, j]
                ohat[index][k - 1][0, np.size(o[index][k - 1])] = 1.0

            der_scalings_square_error[symbol]['intercept'] += \
                energy_coefficient * 2. * (nn_energy - real_energy) / \
                (len(atoms) ** 2.)
            der_scalings_square_error[symbol]['slope'] += \
                energy_coefficient * 2. * \
                (nn_energy - real_energy) * float(o[index][N + 1]) / \
                (len(atoms) ** 2.)
            for k in range(1, N + 2):
                der_weights_square_error[symbol][k] += \
                    energy_coefficient * 2. * \
                    float(nn_energy - real_energy) * \
                    float(scalings[symbol]['slope']) * \
                    np.dot(np.matrix(ohat[index][k - 1]).T,
                           np.matrix(delta[index][k]).T) / \
                    (len(atoms) ** 2.)

        if sfp.train_forces is True:

            real_forces = atoms.get_forces(apply_constraint=False)
            nn_forces = np.zeros((len(atoms), 3))
            for self_atom in atoms:
                self_index = self_atom.index
                der_coordinates_o = {}
                der_coordinates_weights_atomic_output = {}
                n_self_indices = snl.nl_data[(hash_key, self_index)][0]
                n_self_offsets = snl.nl_data[(hash_key, self_index)][1]
                n_symbols = [atoms[n_index].symbol
                             for n_index in n_self_indices]
                for n_symbol, n_index, n_offset in zip(n_symbols,
                                                       n_self_indices,
                                                       n_self_offsets):
                    # for calculating forces, summation runs over neighbor
                    # atoms of type II (within the main cell only)
                    if n_offset[0] == 0 and n_offset[1] == 0 and \
                            n_offset[2] == 0:
                        N = len(o[n_index]) - 2
                        for i in range(3):
                            der_coordinates_o[(n_index, i)] = {}
                            der_indexfp = \
                                sfp.der_fp_data[(hash_key,
                                                 (n_index, self_index, i))]
                            # fingerprint derivatives are scaled
                            scaled_der_indexfp = [None] * len(der_indexfp)
                            count = 0
                            for p in range(len(der_indexfp)):
                                if (sfp.fingerprints_range[n_symbol][p][1] -
                                        sfp.fingerprints_range
                                        [n_symbol][p][0]) > (10.**(-8.)):
                                    scaled_value = 2. * der_indexfp[p] / \
                                        (sfp.fingerprints_range
                                         [n_symbol][p][1] -
                                         sfp.fingerprints_range
                                         [n_symbol][p][0])
                                else:
                                    scaled_value = der_indexfp[p]
                                scaled_der_indexfp[count] = scaled_value
                                count += 1
                            der_coordinates_o[(n_index, i)][0] = \
                                scaled_der_indexfp
                            for k in range(1, N + 2):
                                # Calculating derivative of NN outputs w.r.t.
                                # coordinates
                                temp = \
                                    np.dot(
                                        np.matrix(
                                            der_coordinates_o[
                                                (n_index, i)][k - 1]),
                                        W[n_symbol][k])
                                der_coordinates_o[(n_index, i)][k] = [None] * \
                                    np.size(o[n_index][k])
                                count = 0
                                for j in range(np.size(o[n_index][k])):
                                    # linear function
                                    if activation == 'linear':
                                        der_coordinates_o[(n_index, i)][
                                            k][count] = float(temp[0, j])
                                    # sigmoid function
                                    elif activation == 'sigmoid':
                                        der_coordinates_o[(n_index, i)][
                                            k][count] = \
                                            float(o[n_index][k][0, j] *
                                                  (1. - o[n_index][k][0, j])) \
                                            * float(temp[0, j])
                                    elif activation == 'tanh':  # tanh function
                                        der_coordinates_o[(n_index, i)][
                                            k][count] = float(1. - o[n_index][
                                                k][0, j] *
                                                o[n_index][k][0, j]) * \
                                            float(temp[0, j])
                                    count += 1
                            der_coordinates_D = {}
                            for k in range(1, N + 2):
                                # Calculating coordinate derivative of D matrix
                                der_coordinates_D[k] = \
                                    np.zeros(shape=(np.size(o[n_index][k]),
                                                    np.size(o[n_index][k])))
                                for j in range(np.size(o[n_index][k])):
                                    if activation == 'linear':  # linear
                                        der_coordinates_D[k][j, j] = 0.
                                    elif activation == 'tanh':  # tanh
                                        der_coordinates_D[k][j, j] = \
                                            - 2. * o[n_index][k][0, j] * \
                                            der_coordinates_o[
                                            (n_index, i)][k][j]
                                    elif activation == 'sigmoid':  # sigmoid
                                        der_coordinates_D[k][j, j] = \
                                            der_coordinates_o[
                                            (n_index, i)][k][j] \
                                            - 2. * o[n_index][k][0, j] * \
                                            der_coordinates_o[
                                            (n_index, i)][k][j]
                            # Calculating coordinate derivative of delta
                            der_coordinates_delta = {}
                            # output layer
                            der_coordinates_delta[N + 1] = \
                                der_coordinates_D[N + 1]
                            # hidden layers
                            temp1 = {}
                            temp2 = {}
                            for k in range(N, 0, -1):
                                temp1[k] = np.dot(W[n_symbol][k + 1],
                                                  delta[n_index][k + 1])
                                temp2[k] = np.dot(W[n_symbol][k + 1],
                                                  der_coordinates_delta[k + 1])
                                der_coordinates_delta[k] = \
                                    np.dot(der_coordinates_D[k], temp1[k]) + \
                                    np.dot(D[n_index][k], temp2[k])
                            # Calculating coordinate derivative of ohat and
                            # coordinates weights derivative of atomic_output
                            der_coordinates_ohat = {}
                            der_coordinates_weights_atomic_output[
                                (n_index, i)] = {}
                            for k in range(1, N + 2):
                                der_coordinates_ohat[k - 1] = [None] * \
                                    (1 + len(der_coordinates_o[(n_index, i)][
                                        k - 1]))
                                count = 0
                                for j in range(len(der_coordinates_o[
                                        (n_index, i)][k - 1])):
                                    der_coordinates_ohat[k - 1][count] = \
                                        der_coordinates_o[
                                            (n_index, i)][k - 1][j]
                                    count += 1
                                der_coordinates_ohat[k - 1][count] = 0.
                                der_coordinates_weights_atomic_output[
                                    (n_index, i)][k] = \
                                    np.dot(
                                    np.matrix(der_coordinates_ohat[k - 1]).T,
                                    np.matrix(delta[n_index][k]).T) + \
                                    np.dot(np.matrix(ohat[n_index][k - 1]).T,
                                           np.matrix(
                                           der_coordinates_delta[k]).T)

                            nn_forces[self_index][i] += \
                                - float(scalings[n_symbol]['slope']) * \
                                der_coordinates_o[(n_index, i)][N + 1][0]

                for i in range(3):
                    force_square_error += \
                        ((1.0 / 3.0) * (nn_forces[self_index][i] -
                                        real_forces[self_index][i]) **
                         2.) / len(atoms)

                    for n_symbol, n_index, n_offset in zip(n_symbols,
                                                           n_self_indices,
                                                           n_self_offsets):
                        if n_offset[0] == 0 and n_offset[1] == 0 and \
                                n_offset[2] == 0:
                            N = len(o[n_index]) - 2
                            for k in range(1, N + 2):
                                der_weights_square_error[n_symbol][k] += \
                                    force_coefficient * (2.0 / 3.0) * \
                                    float(scalings[n_symbol]['slope']) * \
                                    (- nn_forces[self_index][i] +
                                     real_forces[self_index][i]) * \
                                    der_coordinates_weights_atomic_output[
                                    (n_index, i)][k] / len(atoms)
                            der_scalings_square_error[n_symbol]['slope'] += \
                                force_coefficient * (2.0 / 3.0) * \
                                der_coordinates_o[(n_index, i)][N + 1][0] * \
                                (- nn_forces[self_index][i] +
                                 real_forces[self_index][i]) / len(atoms)

    ravel = RavelVariables(der_weights_square_error, der_scalings_square_error)
    der_variables_square_error = ravel.to_vector(der_weights_square_error,
                                                 der_scalings_square_error)

    del hashes, images

    queue.put([energy_square_error,
               force_square_error,
               der_variables_square_error])

##############################################################################


def calculate_fingerprints_range(cf, elements, atoms):
    """function to calculate fingerprints range.
    inputs:
        cf: object of CalculateFingerprints class
        elements: list if all elements of atoms
        atoms: ASE atom object
    output:
        fingerprints_range: range of fingerprints of atoms"""

    fingerprint_values = {}
    for element in elements:
        fingerprint_values[element] = {}
        for i in range(len(cf.Gs[element])):
            fingerprint_values[element][i] = []

    for atom in atoms:
        index = atom.index
        symbol = atom.symbol
        indexfp = cf.index_fingerprint(symbol, index)
        for i in range(len(cf.Gs[symbol])):
            fingerprint_values[symbol][i].append(indexfp[i])

    fingerprints_range = {}
    for element in elements:
        fingerprints_range[element] = [None] * len(cf.Gs[element])
        count = 0
        for i in range(len(cf.Gs[element])):
            if len(fingerprint_values[element][i]) > 0:
                minimum = min(fingerprint_values[element][i])
                maximum = max(fingerprint_values[element][i])
            else:
                minimum = -1.0
                maximum = -1.0
            fingerprints_range[element][count] = [minimum, maximum]
            count += 1

    return fingerprints_range

##############################################################################


def compare_train_test_fingerprints(cf, atoms, fingerprints_range):
    """function to compare train images with the test image and decide whether
    the prediction is interpolation or extrapolation.
    inputs:
        cf: object of CalculateFingerprints class
        atoms: ASE atom object
        fingerprints_range: range of fingerprints of the train images
    output:
        compare_train_test_fingerprints:
        integer: zero for interpolation, and one for extrapolation"""

    compare_train_test_fingerprints = 0

    for atom in atoms:
        index = atom.index
        symbol = atom.symbol
        indexfp = cf.index_fingerprint(symbol, index)
        for i in range(len(indexfp)):
            if indexfp[i] < fingerprints_range[symbol][i][0] or \
                    indexfp[i] > fingerprints_range[symbol][i][1]:
                compare_train_test_fingerprints = 1
                break
    return compare_train_test_fingerprints

##############################################################################


def interpolate_images(images, json, fortran):
    """Function to remove extrapolation images from the "images" set based on
    json data"""

    json_file = paropen(json, 'rb')

    parameters = load_parameters(json_file)

    fingerprints_range = parameters['fingerprints_range']
    cutoff = parameters['cutoff']
    Gs = parameters['Gs']

    if isinstance(images, str):
        extension = os.path.splitext(images)[1]
        if extension == '.traj':
            images = io.Trajectory(images, 'r')
        elif extension == '.db':
            images = io.read(images)

    # Images is converted to dictionary form; key is hash of image.
    dict_images = {}
    for image in images:
        key = hash_image(image)
        dict_images[key] = image
    images = dict_images.copy()
    del dict_images

    # Dictionary of interpolated images set is initialized
    interpolated_images = {}
    for hash_key, image in images.keys():
        atoms = image
        _nl = NeighborList(cutoffs=([cutoff / 2.] * len(atoms)),
                           self_interaction=False,
                           bothways=True,
                           skin=0.)
        _nl.update(atoms)
        cf = CalculateFingerprints(cutoff, Gs, atoms, _nl, fortran)
        compare_train_test_fingerprints = 0
        for atom in atoms:
            index = atom.index
            symbol = atom.symbol
            indexfp = cf.index_fingerprint(symbol, index)
            for i in range(len(indexfp)):
                if indexfp[i] < fingerprints_range[symbol][i][0] or \
                        indexfp[i] > fingerprints_range[symbol][i][1]:
                    compare_train_test_fingerprints = 1
                    break
        if compare_train_test_fingerprints == 0:
            interpolated_images[hash_key] = image

    del images

    return interpolated_images

##############################################################################


def send_images_data_to_f90_code(hash_keys, images, sfp, snl, elements,
                                 hiddenlayers, train_forces,
                                 energy_coefficient, force_coefficient, log,
                                 fingerprints_range, no_procs, no_sub_images):
    """Function to send images data to fortran90 code"""

    log('Re-shaping data to send to fortran90...')
    log.tic()

    min_fingerprints = [[fingerprints_range[elm][i][0]
                         for i in range(len(fingerprints_range[elm]))]
                        for elm in elements]
    max_fingerprints = [[fingerprints_range[elm][i][1] for i in
                         range(len(fingerprints_range[elm]))]
                        for elm in elements]

    no_of_images = len(images)
    no_of_elements = len(elements)
    elements_numbers = [atomic_numbers[elm] for elm in elements]
    len_fingerprints_of_elements = [len(sfp.Gs[elm]) for elm in elements]
    no_layers_of_elements = []
    for elm in elements:
        if isinstance(hiddenlayers[elm], int):
            no_layers_of_elements.append(3)
        else:
            no_layers_of_elements.append(len(hiddenlayers[elm]) + 2)

    nn_structure = OrderedDict()
    for elm in elements:
        if isinstance(hiddenlayers[elm], int):
            nn_structure[elm] = ([len(sfp.Gs[elm])] +
                                 [hiddenlayers[elm]] + [1])
        else:
            nn_structure[elm] = ([len(sfp.Gs[elm])] +
                                 [layer for layer in hiddenlayers[elm]] + [1])
    no_nodes_of_elements = [nn_structure[elm][i] for elm in elements
                            for i in range(len(nn_structure[elm]))]
    len_of_no_nodes_of_elements = len(no_nodes_of_elements)

    real_energies = [images[hash_key].get_potential_energy(
        apply_constraint=False) for hash_key in hash_keys]
    no_of_atoms_of_images = [len(images[hash_key]) for hash_key in hash_keys]
    atomic_numbers_of_images = [atomic_numbers[atom.symbol]
                                for hash_key in hash_keys
                                for atom in images[hash_key]]
    raveled_fps_of_images = ravel_fingerprints_of_images(hash_keys,
                                                         images,
                                                         sfp)

    if train_forces is True:
        real_forces = [images[hash_key].get_forces(apply_constraint=False)[
            index] for hash_key in hash_keys
            for index in range(len(images[hash_key]))]

        (list_of_no_of_neighbors,
         raveled_neighborlists,
         raveled_der_fingerprints) = \
            ravel_neighborlists_and_der_fingerprints_of_images(hash_keys,
                                                               images,
                                                               sfp,
                                                               snl)

    log(' ...data re-shaped.', toc=True)

    log('Sending data to fortran90...')
    log.tic()

    fmodules.images_data.energy_coefficient = energy_coefficient
    fmodules.images_data.force_coefficient = force_coefficient
    fmodules.images_data.no_of_images = no_of_images
    fmodules.images_data.no_of_elements = no_of_elements
    fmodules.images_data.elements_numbers = elements_numbers
    fmodules.images_data.no_layers_of_elements = no_layers_of_elements
    fmodules.images_data.no_nodes_of_elements = no_nodes_of_elements
    fmodules.images_data.len_of_no_nodes_of_elements = \
        len_of_no_nodes_of_elements
    fmodules.images_data.real_energies = real_energies
    fmodules.images_data.no_of_atoms_of_images = no_of_atoms_of_images
    fmodules.images_data.atomic_numbers_of_images = atomic_numbers_of_images
    fmodules.images_data.raveled_fps_of_images = raveled_fps_of_images
    fmodules.images_data.len_fingerprints_of_elements = \
        len_fingerprints_of_elements
    fmodules.images_data.train_forces = train_forces
    fmodules.images_data.no_procs = no_procs
    fmodules.images_data.no_sub_images = no_sub_images
    fmodules.images_data.min_fingerprints = min_fingerprints
    fmodules.images_data.max_fingerprints = max_fingerprints

    del real_energies, no_of_atoms_of_images, atomic_numbers_of_images, \
        raveled_fps_of_images, hash_keys, images

    if train_forces is True:
        fmodules.images_data.real_forces = real_forces
        fmodules.images_data.list_of_no_of_neighbors = list_of_no_of_neighbors
        fmodules.images_data.raveled_neighborlists = raveled_neighborlists
        fmodules.images_data.raveled_der_fingerprints = \
            raveled_der_fingerprints

        del real_forces, list_of_no_of_neighbors, raveled_neighborlists,
        raveled_der_fingerprints, min_fingerprints, max_fingerprints

    log(' ...data sent to fortran90.', toc=True)

##############################################################################


def send_image_energy_data_to_f90_code(image, cf, elements,
                                       hiddenlayers, fingerprints_range):
    """Function to send image data to fortran90 code for
    get_potential_energy"""

    min_fingerprints = []
    max_fingerprints = []
    for elm in elements:
        min_fps = []
        max_fps = []
        for i in range(len(fingerprints_range[elm])):
            min_fps.append(fingerprints_range[elm][i][0])
            max_fps.append(fingerprints_range[elm][i][1])
        min_fingerprints.append(min_fps)
        max_fingerprints.append(max_fps)

    no_of_atoms = len(image)
    no_of_elements = len(elements)
    elements_numbers = [atomic_numbers[elm] for elm in elements]
    len_fingerprints_of_elements = [len(cf.Gs[elm]) for elm in elements]
    no_layers_of_elements = []
    for elm in elements:
        if isinstance(hiddenlayers[elm], int):
            no_layers_of_elements.append(3)
        else:
            no_layers_of_elements.append(len(hiddenlayers[elm]) + 2)

    nn_structure = OrderedDict()
    for elm in elements:
        if isinstance(hiddenlayers[elm], int):
            nn_structure[elm] = ([len(cf.Gs[elm])] +
                                 [hiddenlayers[elm]] + [1])
        else:
            nn_structure[elm] = ([len(cf.Gs[elm])] +
                                 [layer for layer in hiddenlayers[elm]] + [1])
    no_nodes_of_elements = []
    for elm in elements:
        for i in range(len(nn_structure[elm])):
            no_nodes_of_elements.append(nn_structure[elm][i])

    raveled_fingerprints = []
    for atom in image:
        index = atom.index
        symbol = atom.symbol
        indexfp = cf.index_fingerprint(symbol, index)
        raveled_fingerprints.append(indexfp)

    atomic_numbers_of_image = [atomic_numbers[atom.symbol] for atom in image]

    fmodules.image_energy_data.no_of_elements = no_of_elements
    fmodules.image_energy_data.no_of_atoms = no_of_atoms
    fmodules.image_energy_data.elements_numbers = elements_numbers
    fmodules.image_energy_data.no_layers_of_elements = no_layers_of_elements
    fmodules.image_energy_data.no_nodes_of_elements = no_nodes_of_elements
    fmodules.image_energy_data.atomic_numbers_of_image = \
        atomic_numbers_of_image
    fmodules.image_energy_data.raveled_fps_of_image = raveled_fingerprints
    fmodules.image_energy_data.len_fingerprints_of_elements = \
        len_fingerprints_of_elements
    fmodules.image_energy_data.min_fingerprints = min_fingerprints
    fmodules.image_energy_data.max_fingerprints = max_fingerprints

    del atomic_numbers_of_image, raveled_fingerprints, min_fingerprints,
    max_fingerprints

##############################################################################


def send_image_force_data_to_f90_code(image, cf, elements,
                                      hiddenlayers, fingerprints_range):
    """Function to send image data to fortran90 code for get_forces"""

    min_fingerprints = []
    max_fingerprints = []
    for elm in elements:
        min_fps = []
        max_fps = []
        for i in range(len(fingerprints_range[elm])):
            min_fps.append(fingerprints_range[elm][i][0])
            max_fps.append(fingerprints_range[elm][i][1])
        min_fingerprints.append(min_fps)
        max_fingerprints.append(max_fps)

    no_of_atoms = len(image)
    no_of_elements = len(elements)
    elements_numbers = [atomic_numbers[elm] for elm in elements]
    len_fingerprints_of_elements = [len(cf.Gs[elm]) for elm in elements]
    no_layers_of_elements = []
    for elm in elements:
        if isinstance(hiddenlayers[elm], int):
            no_layers_of_elements.append(3)
        else:
            no_layers_of_elements.append(len(hiddenlayers[elm]) + 2)

    nn_structure = OrderedDict()
    for elm in elements:
        if isinstance(hiddenlayers[elm], int):
            nn_structure[elm] = ([len(cf.Gs[elm])] +
                                 [hiddenlayers[elm]] + [1])
        else:
            nn_structure[elm] = ([len(cf.Gs[elm])] +
                                 [layer for layer in hiddenlayers[elm]] + [1])
    no_nodes_of_elements = []
    for elm in elements:
        for i in range(len(nn_structure[elm])):
            no_nodes_of_elements.append(nn_structure[elm][i])

    raveled_fingerprints = []
    for atom in image:
        index = atom.index
        symbol = atom.symbol
        indexfp = cf.index_fingerprint(symbol, index)
        raveled_fingerprints.append(indexfp)

    atomic_numbers_of_image = [atomic_numbers[atom.symbol] for atom in image]

    list_of_no_of_neighbors = []
    raveled_neighborlists = []
    raveled_der_fingerprints = []

    nl = NeighborList(cutoffs=([cf.cutoff / 2.] * len(image)),
                      self_interaction=False,
                      bothways=True, skin=0.)
    # FIXME: Is update necessary?
    nl.update(image)
    for self_atom in image:
        self_index = self_atom.index
        neighbor_indices, neighbor_offsets = nl.get_neighbors(self_index)
        n_self_indices = np.append(self_index, neighbor_indices)
        if len(neighbor_offsets) == 0:
            n_self_offsets = [[0, 0, 0]]
        else:
            n_self_offsets = np.vstack(([[0, 0, 0]], neighbor_offsets))
        n_self_symbols = [image[n_index].symbol for
                          n_index in n_self_indices]
        count = 0
        for n_symbol, n_index, n_offset in zip(n_self_symbols, n_self_indices,
                                               n_self_offsets):
            # Only neighboring atoms of type II (within the main cell) are sent
            # to fortran for force calculations
            if n_offset[0] == 0 and n_offset[1] == 0 and n_offset[2] == 0:
                count += 1
                raveled_neighborlists.append(n_index)
                for i in range(3):
                    self_n_index_der_fp = cf.get_der_fingerprint(n_symbol,
                                                                 n_index,
                                                                 self_index,
                                                                 i)
                    raveled_der_fingerprints.append(self_n_index_der_fp)
        list_of_no_of_neighbors.append(count)

    fmodules.image_force_data.no_of_elements = no_of_elements
    fmodules.image_force_data.no_of_atoms = no_of_atoms
    fmodules.image_force_data.elements_numbers = elements_numbers
    fmodules.image_force_data.no_layers_of_elements = no_layers_of_elements
    fmodules.image_force_data.no_nodes_of_elements = no_nodes_of_elements
    fmodules.image_force_data.atomic_numbers_of_image = \
        atomic_numbers_of_image
    fmodules.image_force_data.raveled_fps_of_image = raveled_fingerprints
    fmodules.image_force_data.len_fingerprints_of_elements = \
        len_fingerprints_of_elements
    fmodules.image_force_data.min_fingerprints = min_fingerprints
    fmodules.image_force_data.max_fingerprints = max_fingerprints
    fmodules.image_force_data.list_of_no_of_neighbors = \
        list_of_no_of_neighbors
    fmodules.image_force_data.raveled_neighborlists = raveled_neighborlists
    fmodules.image_force_data.raveled_der_fingerprints = \
        raveled_der_fingerprints

    del atomic_numbers_of_image, raveled_fingerprints, min_fingerprints,
    max_fingerprints, list_of_no_of_neighbors, raveled_neighborlists,
    raveled_der_fingerprints

##############################################################################


def ravel_fingerprints_of_images(hash_keys, images, sfp):
    """Reshape fingerprints of all images into a matrix"""

    keys = [(hash_key, index) for hash_key in hash_keys
            for index in range(len(images[hash_key]))]

    raveled_fingerprints = [sfp.fp_data[key] for key in keys]

    del hash_keys, images, keys

    return raveled_fingerprints

##############################################################################


def ravel_neighborlists_and_der_fingerprints_of_images(hash_keys,
                                                       images, sfp, snl):
    """Reshape neighborlists and derivatives of fingerprints of all images into
    a matrix"""

    list_of_no_of_neighbors = []
    raveled_neighborlists = []
    raveled_der_fingerprints = []
    for hash_key in hash_keys:
        atoms = images[hash_key]
        for self_atom in atoms:
            self_index = self_atom.index
            n_self_indices = snl.nl_data[(hash_key, self_index)][0]
            n_self_offsets = snl.nl_data[(hash_key, self_index)][1]
            count = 0
            for n_index, n_offset in zip(n_self_indices, n_self_offsets):
                # Only neighboring atoms of type II (within the main cell)
                # needs to be sent to fortran for force training
                if n_offset[0] == 0 and n_offset[1] == 0 and n_offset[2] == 0:
                    count += 1
                    raveled_neighborlists.append(n_index)
                    for i in range(3):
                        self_n_index_der_fp = \
                            sfp.der_fp_data[(hash_key,
                                             (n_index, self_index, i))]
                        raveled_der_fingerprints.append(self_n_index_der_fp)
            list_of_no_of_neighbors.append(count)

    del hash_keys, images

    return (list_of_no_of_neighbors,
            raveled_neighborlists,
            raveled_der_fingerprints)

##############################################################################
##############################################################################
##############################################################################


def now():
    """Return string of current time."""
    return datetime.now().isoformat().split('.')[0]

##############################################################################
##############################################################################
##############################################################################
