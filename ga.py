#!/usr/bin/env python

#SBATCH --time=50:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --partition=batch
#SBATCH --constraint="e5-2600"

###############################################################

import numpy as np
from matplotlib import pyplot as plt
import random
import warnings
from ase import Atoms, Atom
from amp import Amp
from ase.calculators.emt import EMT
from amp.regression.neuralnetwork import (make_weight_matrices,
make_scalings_matrices, _RavelVariables, NeuralNetwork)
from amp.utilities import hash_image

###############################################################

class GeneticAlgorithm(object):

    def __init__(self, genetics):

        self.genetics = genetics

    def evolve(self):

        population = self.genetics.initial()

        while True:
            fits_pops = \
            [(self.genetics.show_fitness(variables), variables) for  \
            variables in population]

            if self.genetics.check_stop(fits_pops): 
                break
            if self.genetics.counter % 10 == 0 and  \
            self.genetics.best == 0:
                break
            self.genetics.perturb() # trying to get out of local minima
            population = self.next(fits_pops)

        self.genetics.plt.grid()
        self.genetics.fig.savefig('ga_plot.pdf')

            
        return population

    def next(self, fits):

        parents_generator_object =  \
        self.genetics.parents(self.genetics.weed_out(fits))
        pop_size = len(fits)
        next_gens = []
        while len(next_gens) < pop_size:
            parents = parents_generator_object.next()
            cross = (random.random() < self.genetics.probability_crossover())
            # potentially cross
            children = self.genetics.crossover(parents) if cross else parents
            for ch in children:
                # potentially mutate
                mutate = (random.random() < self.genetics.probability_mutation())
                next_gens.append(self.genetics.mutation(ch) if mutate else ch)

        return next_gens 

     
class GeneticOperations(object):

    def __init__(self, images, activation='tanh',
                 limit=200, pop_size=20, hiddenlayers=(5,5),
                 prob_crossover=0.9, prob_mutation=0.6, 
                 elitism_threshold=0.5, plot=plt):

        self.elitism_threshold = elitism_threshold
        self.counter = 0
        self.plt = plot
        self.fig = self.plt.figure(figsize=(10.,10.))
        self.ax = self.fig.add_subplot(111)
        self.ax.set_xlabel('Generation #')
        self.ax.set_ylabel('Cost Function Value')
        self.limit = limit
        self.pop_size = pop_size
        self.prob_crossover = prob_crossover
        self.prob_mutation = prob_mutation
        self.no_atoms = len(images[0])
        self.activation = activation
        self.pop = []
        self.cost_fxn_track = []
        self.list_images = images

        # Hash the images into a dictionary

        dictimages = {}

        for image in self.list_images:
            hash_key = hash_image(image)
            if hash_key in dictimages.keys():
                warnings.warn('Duplicate image found!')
            dictimages[hash_key] = image

        self.images = dictimages.copy()
        del dictimages

        self.hiddenlayers = hiddenlayers

    
    def probability_crossover(self): # Checked

        return self.prob_crossover

    def probability_mutation(self): # Checked

        return self.prob_mutation

    def initial(self): # Checked

        for i in range(self.pop_size):
            weights = make_weight_matrices(self.hiddenlayers,
                self.activation, self.no_atoms)
            scalings = make_scalings_matrices(self.images,
            self.activation)
            self.ravel = _RavelVariables(self.hiddenlayers,
            no_of_atoms=self.no_atoms)
            variables = self.ravel.to_vector(weights, scalings)
            self.pop.append(variables)

        return self.pop

    def show_fitness(self, variables): # Checked

        # smaller is better, matched == 0
        dict_cost_fxn = {}

        weights, scalings = self.ravel.to_dicts(variables)
        calc = Amp(fingerprint=None, regression= \
            NeuralNetwork(hiddenlayers=self.hiddenlayers,
                          activation=self.activation,
                          weights=weights,
                          scalings=scalings))

        calc.train(self.list_images, energy_goal=10.**10.,
                    force_goal=10.**10.,
                    read_fingerprints=False,
                    overwrite=True)

        return calc.cost_function

    def check_stop(self, fits_populations): # Checked
        
        self.counter += 1

        if self.counter % 10 == 0:

            best_guess = sorted(fits_populations, key=lambda tup: tup[0])[0][1]
            fits = [f for (f, variables) in fits_populations]
            self.best = min(fits)
            self.cost_fxn_track.append(self.best)
            self.ax.plot(self.counter, self.best, 'b*')
            worst = max(fits)
            mean = sum(fits) / len(fits)
            
            print "[count %s] = (%4.1f, %4.1f, %4.1f)\n" %  \
            (str(self.counter), mean, self.best, worst)
            print "Best Initial Guess:", best_guess
            
        return (self.counter >= self.limit)


    def weed_out(self, fits_populations):

        elitist_pop_len = int(self.elitism_threshold *  \
        len(fits_populations))
        ranked_list = sorted(fits_populations, key=lambda tup: tup[0])
        elitist_list = ranked_list[:elitist_pop_len]

        return elitist_list


    def perturb(self):

        if self.counter % 10 == 0 and self.best \
        in self.cost_fxn_track:
            self.prob_mutation += 0.1

        return self.prob_mutation

    def parents(self, fits_populations): # Checked

        while True:
            father = self.fight(fits_populations)
            mother = self.fight(fits_populations)
            yield (father, mother) 

    def crossover(self, parents): # Checked

        father, mother = parents
        index1 = random.randint(1, len(father) - 1)
        index2 = random.randint(1, len(mother) - 1)
        if index1 > index2: # make sure index1 smaller than index2
            index1, index2 = index2, index1
        # slice and cross
        child1 = list(father)[:index1] + list(mother)[index1:index2] +  \
        list(father)[index2:]
        child2 = list(mother)[:index1] + list(father)[index1:index2] +  \
        list(mother)[index2:]

        child1 = np.array(child1)
        child2 = np.array(child2)

        return (child1, child2)

    def mutation(self, child): # Checked

        index = random.randint(0, len(child) - 1)
        vary = random.randint(-5, 5)
        child[index] += vary

        return child
    
    def fight(self, fits_populations): # Checked

        foof, foo = self.sample_random(fits_populations)
        barf, bar = self.sample_random(fits_populations)
        return foo if foof < barf else bar

    def sample_random(self, fits_populations): # Checked

        return fits_populations[random.randint(0, len(fits_populations) - 1)]
    
###########################################################


if __name__ == '__main__':

    training_images = []

    def generate_images(count):

        for step in range(count):
            '''Generates five images with varying configurations'''
            image = Atoms([Atom('Pd', (0., 0., 0.)), Atom('O', (0., 2., 0.)),
            Atom('Pd', (0., 0., 3.)), Atom('Pd', (1., 0., 0.))])

            if step > 0:
                image[step - 1].position =  \
                image[step - 1].position + np.array([0., 1., 1.])

            image.set_calculator(EMT())
            image.get_potential_energy(apply_constraint=False)
            image.get_forces(apply_constraint=False)
            training_images.append(image)

    # Generate the 'training' images

    generate_images(5)


    GeneticAlgorithm(GeneticOperations(images=training_images, pop_size=20, limit=1000)).evolve()

    





