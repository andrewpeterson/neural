# Development guidelines

## Documentation

This documentation is written in the markdown syntax and is stored in the 'docs' folder of the repository. This is hosted on readthedocs.org, which should automatically rebuild the documentation when the repository on bitbucket is updated.

To edit the documentation: edit within the standard git repository, just as you would edit other code. That is, edits are made on your local copy of the repository then checked in to bitbucket.

### View before commit

To view your changes on your own computer before you commit, use the [mkdocs](http://www.mkdocs.org) package to view your formatted documents before you upload to the master repository. This is installed with pip:

```bash
pip install mkdocs
```

Once you have this installed, just cd into the top level of your directory and start the local webserver:

```bash
cd /path/to/my/repositories/neural
mkdocs serve
```

This will provide you with a local IP address. Click this address and you should see your newest version of your documents formatted identically to how they will look on readthedocs.

### Configuration file

The configuration file for the documents is in the root of the repository and is called 'mkdocs.yml'. This contains a list of all the pages in the documentation; if you add a new page you will need to update this list.
